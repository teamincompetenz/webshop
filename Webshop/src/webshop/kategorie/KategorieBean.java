/*
 * Verantwortlich: Jan Hermann
 */

package webshop.kategorie;

import java.io.Serializable;

public class KategorieBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String bezeichnung;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

}
