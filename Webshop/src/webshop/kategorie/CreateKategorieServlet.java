/*
 * Verantwortlich: Jan Hermann
 */

package webshop.kategorie;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/CreateKategorieServlet")

public class CreateKategorieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Form-Inhalt f�llen
		KategorieBean kategorieBean = new KategorieBean();
		kategorieBean.setBezeichnung(request.getParameter("bezeichnung"));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			persist(kategorieBean);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Kategorie-�bersicht
		response.sendRedirect("ReadAllKategorieServlet");

	}

	// Datenbankeintrag hinzuf�gen
	private void persist(KategorieBean kategorieBean) throws ServletException {

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("INSERT INTO kategorie (bezeichnung) VALUES (?);")) {

			pstmt.setString(1, kategorieBean.getBezeichnung());
			pstmt.executeUpdate();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
