/*
 * Verantwortlich: Jan Hermann
 */

package webshop.kategorie;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/UpdateKategorieServlet")

public class UpdateKategorieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Inhalten f�llen
		Long alteID = Long.valueOf(request.getParameter("alteID"));
		KategorieBean kategorieBean = new KategorieBean();
		kategorieBean.setId(Long.valueOf(request.getParameter("neueID")));
		kategorieBean.setBezeichnung(request.getParameter("bezeichnung"));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			update(kategorieBean, alteID);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Kategorie-�bersicht
		response.sendRedirect("ReadAllKategorieServlet");
	}

	// Datenbankeintrag updaten
	private void update(KategorieBean kategorieBean, Long alteID) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("UPDATE kategorie SET id = ?, bezeichnung = ? WHERE id = ?")) {
			pstmt.setLong(1, kategorieBean.getId());
			pstmt.setString(2, kategorieBean.getBezeichnung());
			pstmt.setLong(3, alteID);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
