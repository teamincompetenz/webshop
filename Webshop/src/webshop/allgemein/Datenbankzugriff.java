/*
 * Verantwortlich: Jan Hermann
 */

package webshop.allgemein;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.sql.DataSource;
import webshop.farben.FarbeBean;
import webshop.kategorie.KategorieBean;
import webshop.produkt.ProduktBean;

public class Datenbankzugriff {

	public static ProduktBean readProdukt(DataSource ds, Long id) throws ServletException {
		ProduktBean produktBean = new ProduktBean();
		produktBean.setId(id);

		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM produkt " + "WHERE id = ?")) {

			pstmt.setLong(1, id);
			try (ResultSet rs = pstmt.executeQuery()) {
				if (rs != null && rs.next()) {
					produktBean.setBezeichnung(rs.getString("bezeichnung"));
					produktBean.setPreis(rs.getBigDecimal("preis"));
					produktBean.setFilename(rs.getString("filename"));
					produktBean.setKategorie(rs.getString("kategorie"));
					produktBean.setFarbe(rs.getString("farbe"));
					produktBean.setHoehe(rs.getInt("hoehe"));
					produktBean.setBreite(rs.getInt("breite"));
					produktBean.setTiefe(rs.getInt("tiefe"));
					produktBean.setGewicht(rs.getBigDecimal("gewicht"));
					produktBean.setBeschreibung(rs.getString("beschreibung"));
					produktBean.setSale(rs.getBoolean("sale"));
					produktBean.setOriginalpreis(rs.getBigDecimal("originalpreis"));
					produktBean.setLagerbestand(rs.getInt("lagerbestand"));

					// Bildbehandlung Code von Timothy Groote
					// https://stackoverflow.com/questions/6662432/easiest-way-to-convert-a-blob-into-a-byte-array
					Blob bild = rs.getBlob("file");
					int length = (int) bild.length();
					byte[] bildAlsByte = bild.getBytes(1, length);
					produktBean.setImage(bildAlsByte);
					bild.free();
					// Ende der Codeentnahme
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		return produktBean;
	}

	public static List<KategorieBean> alleKategorien(DataSource ds) throws ServletException {
		List<KategorieBean> kbList = new ArrayList<KategorieBean>();

		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM kategorie");
				ResultSet rs = pstmt.executeQuery()) {

			// List mit ResultSet Ergebnissen f�llen
			while (rs.next()) {
				KategorieBean kategorieBean = new KategorieBean();
				kategorieBean.setBezeichnung(rs.getString("bezeichnung"));
				kategorieBean.setId(rs.getLong("id"));
				kbList.add(kategorieBean);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		return kbList;
	}

	public static List<FarbeBean> alleFarben(DataSource ds) throws ServletException {
		List<FarbeBean> fbList = new ArrayList<FarbeBean>();

		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM farbe");
				ResultSet rs = pstmt.executeQuery()) {

			// List mit ResultSet Ergebnissen f�llen
			while (rs.next()) {
				FarbeBean farbeBean = new FarbeBean();
				farbeBean.setId(rs.getLong("id"));
				farbeBean.setBezeichnung(rs.getString("bezeichnung"));
				fbList.add(farbeBean);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return fbList;
	}
}
