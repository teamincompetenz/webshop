/*
 * Verantwortlich: Jan Hermann
 */

package webshop.allgemein;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Fehlerbehandlung {

	public static void exeptionWeiterleitung(HttpServletRequest request, HttpServletResponse response, String exeptionMessage)
			throws ServletException, IOException {
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/allgemein/fehlerseite.jsp");

		// Parameterübergabe und forward
		request.setAttribute("exeption", "true");
		request.setAttribute("exeptionNachricht", exeptionMessage);
		dispatcher.forward(request, response);
	}
}
