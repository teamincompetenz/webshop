/*
 * Verantwortlich: Daniel Fina
 */

package webshop.login;

import webshop.user.UserBean;
import webshop.allgemein.Fehlerbehandlung;
import webshop.produkt.ProduktBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String mail = request.getParameter("mail");
		String passwort = request.getParameter("passwort");

		// Referer-Adresse als String speichert f�r sp�tere Weiterleitung
		String referer = request.getHeader("referer");
		String submit = request.getParameter("formSubmit");

		// Yes = Man kommt vom Form-Submit | No = AJAX-�berpr�fung der E-Mail
		switch (submit) {
		case "yes":
			UserBean userBean = read(mail);

			// Leeren Warenkorb anlegen
			List<ProduktBean> wk = new ArrayList<ProduktBean>();
			session.setAttribute("warenkorb", wk);
			session.setAttribute("userBean", userBean);

			// Abfrage ob User Admin ist bzw. von welcher Seite er kommt, um Weiterleitung durchzuf�hren
			if (userBean.getAdmin()) {
				response.sendRedirect("jsp/allgemein/admin.jsp");
			} else if (referer.contains("login")) {
				response.sendRedirect("ShopseiteServlet");
			} else {
				response.sendRedirect(referer);
			}
			break;
		case "no":
			if (check(mail, passwort)) {
				response.getWriter().append("true");
			} else {
				response.getWriter().append("false");
			}
			break;
		default:
			response.sendRedirect("index.html");
		}

	}

	// Datenbankabfrage der E-Mail Adresse und Passwort
	private Boolean check(String mail, String passwort) throws ServletException {
		boolean status = false;

		try (Connection con = ds.getConnection();
				PreparedStatement ps = con
						.prepareStatement("SELECT mail,passwort FROM user WHERE mail=? AND passwort=?")) {

			ps.setString(1, mail);
			ps.setString(2, passwort);
			ResultSet rs = ps.executeQuery();
			status = rs.next();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return status;
	}

	// Datenbankabfrage der E-Mail Adresse mittels AJAX
	private UserBean read(String mail) throws ServletException {
		UserBean userBean = new UserBean();

		try (Connection con = ds.getConnection();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM user WHERE mail=?")) {

			ps.setString(1, mail);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs != null && rs.next()) {
					userBean.setNachname(rs.getString("nachname"));
					userBean.setVorname(rs.getString("vorname"));
					userBean.setMail(rs.getString("mail"));
					userBean.setId(rs.getLong("id"));
					userBean.setPasswort(rs.getString("passwort"));
					userBean.setGeburtstag(rs.getDate("geburtstag"));
					userBean.setAdmin(rs.getBoolean("admin"));
				}
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		return userBean;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
