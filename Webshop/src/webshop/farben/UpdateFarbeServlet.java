/*
 * Verantwortlich: Daniel Fina
 */

package webshop.farben;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/UpdateFarbeServlet")

public class UpdateFarbeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Inhalten f�llen
		Long alteID = Long.valueOf(request.getParameter("alteID"));
		FarbeBean farbeBean = new FarbeBean();
		farbeBean.setId(Long.valueOf(request.getParameter("neueID")));
		farbeBean.setBezeichnung(request.getParameter("bezeichnung"));		
		
		// Datenbankzugriff und Fehlerbehandlung
		try {
			update(farbeBean, alteID);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}
        
        // Zur�ck auf die Farbe-�bersicht
		response.sendRedirect("ReadAllFarbeServlet");

	}
	
	private void update(FarbeBean farbeBean,Long alteID) throws ServletException {
		// DB-Zugriff
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("UPDATE farbe SET id = ?, bezeichnung = ? WHERE id = ?")){
			pstmt.setLong(1, farbeBean.getId());
			pstmt.setString(2, farbeBean.getBezeichnung());
			pstmt.setLong(3, alteID);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}
	

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
