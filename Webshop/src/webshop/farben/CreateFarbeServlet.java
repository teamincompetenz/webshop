/*
 * Verantwortlich: Jan Hermann
 */

package webshop.farben;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/CreateFarbeServlet")

public class CreateFarbeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Form-Inhalt f�llen
		FarbeBean farbeBean = new FarbeBean();
		farbeBean.setBezeichnung(request.getParameter("bezeichnung"));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			persist(farbeBean);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Farbe-�bersicht
		response.sendRedirect("ReadAllFarbeServlet");

	}

	// Datenbankeintrag hinzuf�gen
	private void persist(FarbeBean farbeBean) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("INSERT INTO farbe (bezeichnung) VALUES (?);")) {

			pstmt.setString(1, farbeBean.getBezeichnung());
			pstmt.executeUpdate();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
}
