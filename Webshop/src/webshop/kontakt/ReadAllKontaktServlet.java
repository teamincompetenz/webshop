/*
 * Verantwortlich: Daniel Fina
 */

package webshop.kontakt;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/ReadAllKontaktServlet")

public class ReadAllKontaktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String grund = request.getParameter("grund");
		String mail = request.getParameter("mail");
		// Referer-URL als String speichern
		String referer = request.getHeader("referer");
		

		// Liste mit DB-Inhalt f�llen
		List<KontaktBean> kontaktList = suche(grund, mail);

		// Scope "Request"
		request.setAttribute("kontaktliste", kontaktList);

		// Abh�ngig von der Referer-URL die JSP-Datei ausw�hlen
		if (referer.contains("KontaktServlet") && !Boolean.valueOf(request.getParameter("delete"))) {
			final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/kontakt/kontaktTable.jsp");
			dispatcher.forward(request, response);
		} else {
			final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/kontakt/readKontakt.jsp");
			dispatcher.forward(request, response);
		}
	}

	// Datenbankabfrage des Nachnames und der E-Mail
	private List<KontaktBean> suche(String grund, String mail) throws ServletException {
		grund = (grund == null || grund == "") ? "%" : "%" + grund + "%";
		mail = (mail == null || mail == "") ? "%" : "%" + mail + "%";
		List<KontaktBean> kontakte = new ArrayList<KontaktBean>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("SELECT * FROM kontakt WHERE grund LIKE ? AND mail LIKE ?")) {

			pstmt.setString(1, grund);
			pstmt.setString(2, mail);
			try (ResultSet rs = pstmt.executeQuery()) {
				// List mit ResultSet Ergebnissen f�llen
				while (rs.next()) {
					KontaktBean kb = new KontaktBean();
					kb.setId(rs.getLong("id"));
					kb.setGrund(rs.getString("grund"));
					kb.setMail(rs.getString("mail"));
					kb.setVorname(rs.getString("vorname"));
					kb.setNachname(rs.getString("nachname"));
					kb.setTitel(rs.getString("titel"));
					kb.setNachricht(rs.getString("nachricht"));
					kontakte.add(kb);
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return kontakte;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
