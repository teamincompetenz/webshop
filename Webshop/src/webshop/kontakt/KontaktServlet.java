/*
 * Verantwortlich: Daniel Fina
 */

package webshop.kontakt;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/kontaktServlet")
public class KontaktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		
		// KontaktBean bef�llen
		KontaktBean kontaktBean = new KontaktBean();
		kontaktBean.setTitel(request.getParameter("titel"));
		kontaktBean.setNachname(request.getParameter("nachname"));
		kontaktBean.setVorname(request.getParameter("vorname"));
		kontaktBean.setMail(request.getParameter("mail"));
		kontaktBean.setGrund(request.getParameter("grund"));
		kontaktBean.setNachricht(request.getParameter("nachricht"));
		
		// Datenbankzugriff und Fehlerbehandlung
				try {
					persist(kontaktBean);
					session.setAttribute("kontaktBean", kontaktBean);
				} catch (ServletException servex) {
					Fehlerbehandlung.exeptionWeiterleitung(request, response,servex.getMessage());
					return;
				}
		
		
		//Zur Kontaktausgabe per Redirect weiterleiten
		response.sendRedirect("jsp/kontakt/kontaktausgabe.jsp");
		
	}
	
	// Datenbankeintrag hinzuf�gen
	private void persist(KontaktBean kontakt) throws ServletException {
		
		// Name der Spalte, die automatisch generiert wird
		String[] generatedKeys = new String[] {"id"};	
		try (Connection con = ds.getConnection();
			PreparedStatement pstmt = con.prepareStatement(
					"INSERT INTO kontakt (nachricht,grund,mail,vorname,nachname,titel) VALUES (?,?,?,?,?,?)", 
					generatedKeys)){

			pstmt.setString(1, kontakt.getNachricht());
			pstmt.setString(2, kontakt.getGrund());
			pstmt.setString(3, kontakt.getMail());
			pstmt.setString(4, kontakt.getVorname());
			pstmt.setString(5, kontakt.getNachname());
			pstmt.setString(6, kontakt.getTitel());
			pstmt.executeUpdate();
			
			
			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				while (rs.next()) {
					kontakt.setId(rs.getLong(1));
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}
	
	// Methode leitet auf Fehlerseite weiter
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			Fehlerbehandlung.exeptionWeiterleitung(request, response,
					"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
		}
}
