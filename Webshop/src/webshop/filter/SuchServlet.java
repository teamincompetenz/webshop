/*
 * Verantwortlich: Daniel Fina
 */

package webshop.filter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Datenbankzugriff;
import webshop.allgemein.Fehlerbehandlung;
import webshop.farben.FarbeBean;
import webshop.kategorie.KategorieBean;
import webshop.produkt.ProduktBean;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/SuchServlet")
public class SuchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS" )
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	
	// Liste und Suchvariabel initialisieren
	List<ProduktBean> pbList = suche(request.getParameter("suche"));
	List<KategorieBean> kbList = Datenbankzugriff.alleKategorien(ds);
	List<FarbeBean> fbList  = Datenbankzugriff.alleFarben(ds);

	// Scope "Request"
	request.setAttribute("kategorieliste", kbList);
	request.setAttribute("produktliste", pbList);
	request.setAttribute("farbeliste", fbList);
	
	// Suche auf Shopseite weiterleiten
	final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/shopseite.jsp");
	dispatcher.forward(request, response);
	
	}

	// Datenbankabfrage nach dem Suchbegriff
	private List<ProduktBean> suche(String suchbegriff) throws ServletException{
	
		List<ProduktBean> pbList = new ArrayList<ProduktBean>();
		
		try(Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM produkt WHERE bezeichnung LIKE ?");
				){
			suchbegriff = (suchbegriff == null || suchbegriff == "") ? "%" : "%" + suchbegriff + "%";
			
			pstmt.setString(1, suchbegriff);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				ProduktBean pb = new ProduktBean();
				pb.setId(rs.getLong("id"));
				pb.setBezeichnung(rs.getString("bezeichnung"));
				pb.setPreis(rs.getBigDecimal("preis"));
				pb.setSale(rs.getBoolean("sale"));
				pb.setOriginalpreis(rs.getBigDecimal("originalpreis"));
				pb.setFilename(rs.getString("filename"));
				pb.setKategorie(rs.getString("kategorie"));
				pb.setFarbe(rs.getString("farbe"));
				pb.setImage(rs.getBytes("file"));
				pbList.add(pb);
			}
			
		}catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		return pbList;
	}
	
	
	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
	
}
