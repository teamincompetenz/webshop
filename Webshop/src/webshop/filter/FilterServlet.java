/*
 * Verantwortlich: Daniel Fina
 */

package webshop.filter;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Datenbankzugriff;
import webshop.allgemein.Fehlerbehandlung;
import webshop.farben.FarbeBean;
import webshop.kategorie.KategorieBean;
import webshop.produkt.ProduktBean;

@WebServlet("/FilterServlet")
public class FilterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// Liste und Filtervariablen initialisieren
		ArrayList<Object> filter = new ArrayList<>();
		String kategorie = null;
		BigDecimal minPreis = null;
		BigDecimal maxPreis = null;
		String farbe = null;
		String steigung = null;
		String suche = null;

		// Falls ein Wert in den Parametern steht, wird dieser in die Filtervariablen geschrieben
		if (!(request.getParameter("kategorie").isEmpty())) {
			kategorie = request.getParameter("kategorie");
		}
		if (!(request.getParameter("minPreis").isEmpty())) {
			minPreis = BigDecimal.valueOf(Double.valueOf(request.getParameter("minPreis")));
		}
		if (!(request.getParameter("maxPreis").isEmpty())) {
			maxPreis = BigDecimal.valueOf(Double.valueOf(request.getParameter("maxPreis")));
		}
		if (!(request.getParameter("farbe").isEmpty())) {
			farbe = request.getParameter("farbe");
		}
		if (!(request.getParameter("preis").isEmpty())) {
			steigung = request.getParameter("preis");
		}
		if (!(request.getParameter("suche").isEmpty())) {
			suche = request.getParameter("suche");
		}

		// Alle Filtervariablen in die Liste einf�gen
		filter.add(kategorie);
		filter.add(minPreis);
		filter.add(maxPreis);
		filter.add(farbe);
		filter.add(steigung);
		filter.add(suche);

		// Datenbankzugriff und Fehlerbehandlung
		List<ProduktBean> pbList = read(filter);
		List<KategorieBean> kbList = Datenbankzugriff.alleKategorien(ds);
		List<FarbeBean> fbList = Datenbankzugriff.alleFarben(ds);

		// Scope "Request"
		request.setAttribute("produktliste", pbList);
		request.setAttribute("kategorieliste", kbList);
		request.setAttribute("farbeliste", fbList);

		// Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/filterTable.jsp");
		dispatcher.forward(request, response);
	}

	private List<ProduktBean> read(ArrayList<Object> filter) throws ServletException {
		List<ProduktBean> pbList = new ArrayList<ProduktBean>();
		StringBuilder sb = new StringBuilder();

		// Auf Nullwerte �berpr�fen und SQL String zusammensetzen
		if (filter.get(0) != null) {
			sb.append("SELECT * FROM produkt WHERE kategorie = ?");
			if (filter.get(1) != null) {
				sb.append(" AND preis >= ?");
			}
		} else {
			sb.append("SELECT * FROM produkt");
			if (filter.get(1) != null) {
				sb.append(" where preis >= ?");
			} else {
				sb.append(" where preis >= 0");
			}
		}
		if (filter.get(2) != null) {
			sb.append(" AND preis <= ?");
		}
		if (filter.get(3) != null) {
			sb.append(" AND farbe = ?");
		}
		if (filter.get(5) != null) {
			sb.append(" AND bezeichnung LIKE ?");
		}
		if (filter.get(4) != null && filter.get(4).equals("ASC")) {
			sb.append(" ORDER BY preis ASC");
		}
		if (filter.get(4) != null && filter.get(4).equals("DESC")) {
			sb.append(" ORDER BY preis DESC");
		}

		int i = 1;
		// DB-Zugriff
		try (Connection con = ds.getConnection(); PreparedStatement pstmt = con.prepareStatement(sb.toString())) {
			if (filter.get(0) != null) {
				pstmt.setString(i, (String) filter.get(0));
				i++;
			}
			if (filter.get(1) != null) {
				pstmt.setBigDecimal(i, (BigDecimal) filter.get(1));
				i++;
			}
			if (filter.get(2) != null) {
				pstmt.setBigDecimal(i, (BigDecimal) filter.get(2));
				i++;
			}
			if (filter.get(3) != null) {
				pstmt.setString(i, (String) filter.get(3));
				i++;
			}
			if (filter.get(5) != null) {
				String suche = (String) filter.get(5);
				suche = "%" + suche + "%";
				pstmt.setString(i, suche);
			}

			ResultSet rs = pstmt.executeQuery();
			// List mit ResultSet Ergebnissen f�llen
			while (rs.next()) {
				ProduktBean pb = new ProduktBean();
				pb.setId(rs.getLong("id"));
				pb.setBezeichnung(rs.getString("bezeichnung"));
				pb.setPreis(rs.getBigDecimal("preis"));
				pb.setFilename(rs.getString("filename"));
				pb.setKategorie(rs.getString("kategorie"));
				pb.setFarbe(rs.getString("farbe"));
				pb.setImage(rs.getBytes("file"));
				pb.setSale(rs.getBoolean("sale"));
				pb.setOriginalpreis(rs.getBigDecimal("originalpreis"));
				pbList.add(pb);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return pbList;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
}
