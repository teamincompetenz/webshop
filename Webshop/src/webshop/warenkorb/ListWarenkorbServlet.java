/*
 * Verantwortlich: Felix Gr�hlich
 */
package webshop.warenkorb;

import java.io.IOException;
import java.math.BigDecimal;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import webshop.produkt.ProduktBean;

import java.util.List;
/**
 * Servlet implementation class ListWarenkorbProdukte
 */
@WebServlet("/ListWarenkorbServlet")
public class ListWarenkorbServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		
		@SuppressWarnings("unchecked")
		List<ProduktBean> warenkorb = (List<ProduktBean>) session.getAttribute("warenkorb");
		
		if(warenkorb != null) {
			BigDecimal summe = summe(warenkorb);
			request.getSession().setAttribute("summe", summe);
		}
			//Scope "Request"
			request.getSession().setAttribute("warenkorb", warenkorb);
			
		
		
		//Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/warenkorb/warenkorb.jsp");
		dispatcher.forward(request, response);
		
	}
	

	private BigDecimal summe(List<ProduktBean> wk) {
		BigDecimal summe = new BigDecimal("0.00");
		
		for( ProduktBean pb : wk) {
			summe = summe.add(pb.getSummenpreis());
			
			
		}
		
		return summe;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
