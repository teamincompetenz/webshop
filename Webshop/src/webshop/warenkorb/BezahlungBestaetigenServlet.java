/*
 * Verantwortlich: Felix Gr�hlich
 */

package webshop.warenkorb;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class BezahlungBestaetigenServlet
 */
@WebServlet("/BezahlungBestaetigenServlet")
public class BezahlungBestaetigenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BezahlungBestaetigenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		BigDecimal summe = (BigDecimal)request.getSession().getAttribute("summe");
		
		if(request.getParameter("lieferung").equals("standard")) {
			summe = summe.add(BigDecimal.valueOf(45.99));
			request.setAttribute("lieferung", "Standard");
		}else if(request.getParameter("lieferung").equals("express")){
			summe = summe.add(BigDecimal.valueOf(79.99));
			request.setAttribute("lieferung", "Express");
			
		}else {
			response.sendRedirect("jsp/allgemein/fehlerseite.jsp");
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(request.getParameter("strasse")+", "+request.getParameter("plz")+ " " + request.getParameter("ort"));
		
		
	
		
		request.setAttribute("zahlung", request.getParameter("zahlung"));
		request.getSession().setAttribute("lieferSumme", summe);
		request.setAttribute("adresse", sb.toString());
		

		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/warenkorb/bestaetigen.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
