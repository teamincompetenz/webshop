/*
 * Verantwortlich: Felix Gr�hlich
 */
package webshop.warenkorb;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;

/**
 * Application Lifecycle Listener implementation class HttpSessionListener
 *
 */



@WebListener
public class HttpSessionListener implements javax.servlet.http.HttpSessionListener, HttpSessionAttributeListener {

	
	List<HttpSession> sessionlist = new ArrayList<HttpSession>();
	ServletContext sc = null;
	
	
    public List<HttpSession> getSessionlist() {
		return sessionlist;
	}

	public void setSessionlist(List<HttpSession> sessionlist) {
		this.sessionlist = sessionlist;
	}

	/**
     * Default constructor. 
     */
    public HttpSessionListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    
    
    public void sessionCreated(HttpSessionEvent se)  { 
    	
    	List<HttpSession>sl = getSessionlist();
    	
    	sl.add(se.getSession());
    	setSessionlist(sl);
    	ServletContext sc = getSc();
    	if(sc == null) {
        sc = se.getSession().getServletContext();
    	}
    	sc.setAttribute("sessionlist", sl);
    	setSc(sc);
    	
  
    }

	public ServletContext getSc() {
		return sc;
	}

	public void setSc(ServletContext sc) {
		this.sc = sc;
	}

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent se)  { 
    	
    	List<HttpSession>sl = getSessionlist();

    	sl.remove(se.getSession());
    	setSessionlist(sl);
    	ServletContext sc = getSc();
    	sc.setAttribute("sessionlist", sl);
    	setSc(sc);
         // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
     */
    public void attributeAdded(HttpSessionBindingEvent event)  { 
    	
    	List<HttpSession>sl = getSessionlist();

    	int i = 0;
    	for (HttpSession se : sl) {
    		
    		if(se.equals(event.getSession())) {
    			se.setAttribute(event.getName(), event.getValue());
    			sl.set(i, se);
    			ServletContext sc = getSc();
    			sc.setAttribute("sessionlist", sl);
    			setSessionlist(sl);
    			setSc(sc);
    		}
    		i++;
    	}
         // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
     */
    public void attributeRemoved(HttpSessionBindingEvent event)  { 
    	
    	int i = 0;
    	
    	List<HttpSession>sl = getSessionlist();

    	for(HttpSession se : sl) {
    		if(se.equals(event.getSession())) {
    			se.removeAttribute(event.getName());
    			sl.set(i, se);
    			ServletContext sc = getSc();
    			sc.setAttribute("sessionlist", sl);
    			setSessionlist(sl);
    			setSc(sc);
    		}
    		i++;
    	}
         // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionAttributeListener#attributeReplaced(HttpSessionBindingEvent)
     */
    public void attributeReplaced(HttpSessionBindingEvent event)  { 
         // TODO Auto-generated method stub
    	
    	
    		
    	}
    
	
}
