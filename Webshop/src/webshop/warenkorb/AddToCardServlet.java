/*
 * Verantwortlich: Felix Gr�hlich
 */
package webshop.warenkorb;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import webshop.produkt.ProduktBean;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddToCardServlet
 */

@WebServlet("/AddToCardServlet")
public class AddToCardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		final HttpSession session = request.getSession();

		@SuppressWarnings("unchecked")
		List<ProduktBean> warenkorb = (List<ProduktBean>) session.getAttribute("warenkorb");
		Boolean nichtvorhanden = true;
		
		if (warenkorb == null || warenkorb.isEmpty()) {
			warenkorb = new ArrayList<ProduktBean>();
		} else {
			int i = 0;
			

			for (ProduktBean pb : warenkorb) {

				if (pb.getId() == (Long.valueOf(request.getParameter("produktID")))) {
					Long anzahl = Long.valueOf(request.getParameter("anzahl"));
					Long neueAnzahl = pb.getAnzahl() + anzahl;

					if(neueAnzahl.intValue() <= pb.getLagerbestand()) {
				
					pb.setAnzahl(neueAnzahl);
					pb.setSummenpreis(pb.getPreis().multiply(BigDecimal.valueOf(pb.getAnzahl())));

					warenkorb.set(i, pb);
					
					}
					
					nichtvorhanden = false;

					break;
				}
				i++;
			}}
			if (nichtvorhanden) {
				ProduktBean pb = new ProduktBean();
				pb.setId(Long.valueOf(request.getParameter("produktID")));
				Long anzahl = Long.valueOf(request.getParameter("anzahl"));
				pb.setAnzahl(anzahl);
				BigDecimal preis = new BigDecimal(request.getParameter("produktPreis"));
				pb.setPreis(preis);
				preis = preis.multiply(BigDecimal.valueOf(pb.getAnzahl()));
				pb.setSummenpreis(preis);
				pb.setLagerbestand(Integer.valueOf(request.getParameter("lagerbestand")));
				pb.setBezeichnung(String.valueOf(request.getParameter("bezeichnung")));

				warenkorb.add(pb);

			}

		

		request.getSession().setAttribute("warenkorb", warenkorb);

		response.sendRedirect("ShopseiteServlet");
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
