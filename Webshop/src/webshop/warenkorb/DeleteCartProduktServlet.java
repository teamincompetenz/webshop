/*
 * Verantwortlich: Felix Gr�hlich
 */
package webshop.warenkorb;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import webshop.produkt.ProduktBean;

/**
 * Servlet implementation class DeleteCartProduktServlet
 */

@WebServlet("/DeleteCartProduktServlet")
public class DeleteCartProduktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;       
//  

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		final HttpSession session = request.getSession();
		
		@SuppressWarnings("unchecked")
		List<ProduktBean>warenkorb = (List<ProduktBean>)session.getAttribute("warenkorb");
		
		int index = 0;
		for (ProduktBean pb : warenkorb) {
			
			if(pb.getId() == Long.valueOf(request.getParameter("id"))) {
				if(warenkorb.get(index).getAnzahl()==1) {
//					updateAnzahl(pb);
					warenkorb.remove(index);
					break;
				}else {
//				updateAnzahl(pb);
				pb.setLagerbestand(pb.getLagerbestand()+1);
				warenkorb.get(index).setAnzahl(pb.getAnzahl()-1);
				warenkorb.get(index).setSummenpreis(pb.getPreis().multiply(BigDecimal.valueOf(pb.getAnzahl())));
				break;}
			}
			index++;
		}
		
		request.getSession().setAttribute("warenkorb", warenkorb);

//		final RequestDispatcher dispatcher = request.getRequestDispatcher("ListWarenkorbServlet");
//		dispatcher.forward(request, response);
		response.sendRedirect("ListWarenkorbServlet");
	}
	
//	private void updateAnzahl(ProduktBean pb) throws ServletException{
//		try(Connection con = ds.getConnection();
//				PreparedStatement pstmt = con.prepareStatement("UPDATE produkt SET lagerbestand = ? WHERE id = ?")){
//			pstmt.setLong(1, (pb.getLagerbestand()+1));
//			pstmt.setLong(2, pb.getId());
//			pstmt.executeUpdate();
//		} catch (Exception ex) {
//			throw new ServletException(ex.getMessage());
//		}
//		
//	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
