/*
 * Verantwortlich: Felix Gr�hlich
 */
package webshop.warenkorb;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import webshop.auftrag.AuftragBean;
import webshop.auftrag.AuftragspositionBean;
import webshop.produkt.ProduktBean;
import webshop.user.UserBean;

/**
 * Servlet implementation class BezahlenServlet
 */
@WebServlet("/BezahlenServlet")
public class BezahlenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//Bean mit Form-Inhalt f�llen
		AuftragBean auftragBean = new AuftragBean();
		
		HttpSession session = request.getSession();
		Cookie[] cookies = request.getCookies();
		
		UserBean user = (UserBean) session.getAttribute("userBean");
		if(user == null) {
			for (Cookie ck : cookies) {
				if(ck.getName().equals("mail")){
	
					auftragBean.setUserID(ausgabeUserID(ck.getValue()));
				}
				
			}}else {
				auftragBean.setUserID(user.getId());
			}
		BigDecimal summe = (BigDecimal) session.getAttribute("summe");
	
		
		
		auftragBean.setDatum(Date.valueOf(LocalDate.now()));
		auftragBean.setRechnungssumme(summe);
		
		// Produktliste aus Warenkorb
		
		// Datenbankzugriff
		Long auftragsnummer = auftragAnlegen(auftragBean);
				
				@SuppressWarnings("unchecked")
				List<ProduktBean> warenkorb = (List<ProduktBean>) session.getAttribute("warenkorb");
				
				
				// Warenkorb positionen in Datenbank speichern
				for (int i = 0; i < warenkorb.size(); i++) {
					AuftragspositionBean apb = new AuftragspositionBean();
					apb.setPositionsID(Long.valueOf(i+1));
					apb.setAuftragsnummer(auftragsnummer);
					apb.setProduktID(warenkorb.get(i).getId());
					apb.setAnzahl(Math.toIntExact((warenkorb.get(i).getAnzahl())));
					auftragsPositionAnlegen(apb);
				}

		for(ProduktBean pb : warenkorb) {
			updateAnzahl(pb);
		}
		
		
		// Scope "Request"

        request.getSession().removeAttribute("warenkorb");
        request.getSession().removeAttribute("summe");
        
        // Zur�ck auf die Auftrags-�bersicht
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/warenkorb/bestellt.jsp");
		dispatcher.forward(request,response);
	}

	private Long auftragAnlegen(AuftragBean auftragBean) throws ServletException {

		Long ausgabe = null;
		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("INSERT INTO auftrag (userID, datum, rechnungssumme) VALUES (?,?,?);", PreparedStatement.RETURN_GENERATED_KEYS)) {
			
			

			pstmt.setLong(1, auftragBean.getUserID());
			pstmt.setDate(2, auftragBean.getDatum());
			pstmt.setBigDecimal(3, auftragBean.getRechnungssumme());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			while (rs.next()) {
			ausgabe = rs.getLong(1);
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		return ausgabe;
	}
	
	
	
	private void auftragsPositionAnlegen(AuftragspositionBean auftragspositionBean) throws ServletException {

		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("INSERT INTO auftragsposition (positionsID, auftragsnummer, produktID, anzahl) VALUES (?,?,?,?);")) {

			pstmt.setLong(1, auftragspositionBean.getPositionsID());
			pstmt.setLong(2, auftragspositionBean.getAuftragsnummer());
			pstmt.setLong(3, auftragspositionBean.getProduktID());
			pstmt.setInt(4, auftragspositionBean.getAnzahl());
			pstmt.executeUpdate();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}
	
	private void updateAnzahl(ProduktBean pb) throws ServletException{
		try(Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE produkt SET lagerbestand = ? WHERE id = ?")){
			pstmt.setLong(1, (pb.getLagerbestand()-pb.getAnzahl()));
			pstmt.setLong(2, pb.getId());
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
	}
	
	private Long ausgabeUserID(String mail) throws ServletException{
		
		Long id = null;
		
		
		try(Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("Select id From user where mail=?")){
					pstmt.setString(1, mail);
					try (ResultSet rs = pstmt.executeQuery()){
						if(rs.next()) {
							id = rs.getLong("id");
						}
					}
				}catch (Exception ex) {
					throw new ServletException(ex.getMessage());
				}
		return id;
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
