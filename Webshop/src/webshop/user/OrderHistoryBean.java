/*
 * Verantwortlich: Jan Hermann
 */

package webshop.user;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class OrderHistoryBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long auftragsnummer;
	private Long userID;
	private Date datum;
	private BigDecimal rechnungssumme;
	private Long produktID;
	private String bezeichnung;
	private Long anzahl;
	

	public Long getAuftragsnummer() {
		return auftragsnummer;
	}
	public void setAuftragsnummer(Long auftragsnummer) {
		this.auftragsnummer = auftragsnummer;
	}
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public BigDecimal getRechnungssumme() {
		return rechnungssumme;
	}
	public void setRechnungssumme(BigDecimal rechnungssumme) {
		this.rechnungssumme = rechnungssumme;
	}
	public Long getProduktID() {
		return produktID;
	}
	public void setProduktID(Long produktID) {
		this.produktID = produktID;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public Long getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(Long anzahl) {
		this.anzahl = anzahl;
	}
}
