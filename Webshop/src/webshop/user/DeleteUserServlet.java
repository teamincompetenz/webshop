/*
 * Verantwortlich: Felix Gr�hlich, Jan Hermann (Zeile 46 bis 59)
 */
package webshop.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

/**
 * Servlet implementation class DeleteUserServlet
 */
@WebServlet("/DeleteUserServlet")
public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
       private DataSource ds;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.setCharacterEncoding("UTF-8");
		
		Long id = Long.valueOf(request.getParameter("id"));
		
		// Verantwortlich: Jan Hermann
		// L�schen nicht �ber Link m�glich, nur �ber Icons auf CRUD Seite
		if (request.getHeader("referer") == null || !request.getHeader("referer").contains("UserServlet")) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response,
					"Sie sind nicht berechtigt diese Funktion aufzurufen!");
			return;
		}
		
		// Datenbankzugriff und Fehlerbehandlung
		try {
			delete(id);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}
		
		request.setAttribute("id", id);
		
		final RequestDispatcher dispatcher = request.getRequestDispatcher("ReadAllUserServlet");
		dispatcher.forward(request, response);
	
	}
	
	private void delete(Long id) throws ServletException{
		
		try(Connection con = ds.getConnection();
				PreparedStatement pstmt= con.prepareStatement("DELETE FROM user WHERE id = ?")){
			pstmt.setLong(1, id);
			pstmt.executeUpdate();
		}catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
