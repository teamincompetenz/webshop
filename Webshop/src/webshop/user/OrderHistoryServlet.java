/*
 * Verantwortlich: Jan Hermann
 */

package webshop.user;

import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import webshop.allgemein.Fehlerbehandlung;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/OrderHistoryServlet")

public class OrderHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Datenbankzugriff
		Long userId = Long.valueOf(request.getParameter("id"));
		List<OrderHistoryBean> ohbList = read(userId);

		// Request-Scope
		request.setAttribute("orderHistoryList", ohbList);
		request.setAttribute("userID", userId);

		// Weiterleiten auf Bestellübersicht-Seite
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/user/orderHistory.jsp");
		dispatcher.forward(request, response);
	}

	// Datenbankzugriff
	private List<OrderHistoryBean> read(Long userID) throws ServletException {
		List<OrderHistoryBean> ohbList = new ArrayList<OrderHistoryBean>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT auftrag.datum, auftrag.auftragsnummer, auftrag.rechnungssumme, produkt.id, produkt.bezeichnung, auftragsposition.anzahl FROM auftrag " + 
						"JOIN auftragsposition ON auftrag.auftragsnummer=auftragsposition.auftragsnummer " + 
						"JOIN produkt on auftragsposition.produktID=produkt.ID " + 
						"WHERE userID = ? ORDER BY auftrag.datum ASC, auftrag.auftragsnummer ASC")){
				pstmt.setLong(1, userID);
				ResultSet rs = pstmt.executeQuery();

			// Bean füllen und in Liste einfügen
			while (rs.next()) {
				OrderHistoryBean orderHistoryBean = new OrderHistoryBean();
				orderHistoryBean.setUserID(userID);
				orderHistoryBean.setDatum(rs.getDate("auftrag.datum"));
				orderHistoryBean.setAuftragsnummer(rs.getLong("auftrag.auftragsnummer"));
				orderHistoryBean.setRechnungssumme(rs.getBigDecimal("auftrag.rechnungssumme"));
				orderHistoryBean.setProduktID(rs.getLong("produkt.id"));
				orderHistoryBean.setBezeichnung(rs.getString("produkt.bezeichnung"));
				orderHistoryBean.setAnzahl(rs.getLong("auftragsposition.anzahl"));
				ohbList.add(orderHistoryBean);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return ohbList;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
}
