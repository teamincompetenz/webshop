/*
 * Verantwortlich: Daniel Fina
 */

package webshop.user;

import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import webshop.allgemein.Fehlerbehandlung;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/ReadAllUserServlet")

public class ReadAllUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String nachname = request.getParameter("nachname");
		String mail = request.getParameter("mail");

		// Referer-URL als String speichern
		String referer = request.getHeader("referer");
		List<UserBean> userBean = suche(nachname, mail);
		request.setAttribute("userliste", userBean);

		// Abh�ngig von der Referer-URL die JSP-Datei ausw�hlen
		if (referer.contains("UserServlet") && !Boolean.valueOf(request.getParameter("delete"))) {
			final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/user/userTable.jsp");
			dispatcher.forward(request, response);
		} else {
			final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/user/crudUser.jsp");
			dispatcher.forward(request, response);
		}
	}

	// Datenbankabfrage des Nachnames und der E-Mail
	private List<UserBean> suche(String nachname, String mail) throws ServletException {
		nachname = (nachname == null || nachname == "") ? "%" : "%" + nachname + "%";
		mail = (mail == null || mail == "") ? "%" : "%" + mail + "%";
		List<UserBean> user = new ArrayList<UserBean>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("SELECT * FROM user WHERE nachname LIKE ? AND mail LIKE ?")) {

			pstmt.setString(1, nachname);
			pstmt.setString(2, mail);

			try (ResultSet rs = pstmt.executeQuery()) {
				while (rs.next()) {
					UserBean ub = new UserBean();
					ub.setId(rs.getLong("id"));
					ub.setVorname(rs.getString("vorname"));
					ub.setNachname(rs.getString("nachname"));
					ub.setMail(rs.getString("mail"));
					ub.setGeburtstag(rs.getDate("geburtstag"));
					ub.setPasswort(rs.getString("passwort"));
					ub.setAdmin(rs.getBoolean("admin"));
					user.add(ub);
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return user;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
