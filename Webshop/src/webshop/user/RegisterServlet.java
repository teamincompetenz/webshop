/*
 * Verantwortlich: Daniel Fina
 */

package webshop.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	// AJAX-Abfrage der E-Mail
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		String mail = request.getParameter("mail");
		if (checkMail(mail)) {
			response.getWriter().append("false");
		} else {
			response.getWriter().append("true");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");

		// UserBean mit Inhalt f�llen
		UserBean userBean = new UserBean();
		userBean.setNachname(request.getParameter("nachname"));
		userBean.setVorname(request.getParameter("vorname"));
		userBean.setMail(request.getParameter("mail"));
		userBean.setGeburtstag(Date.valueOf(request.getParameter("geburtstag")));
		userBean.setPasswort(request.getParameter("passwortRegister"));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			persist(userBean);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Weiterleitung auf die Login-Seite nach Registrierung
		response.sendRedirect("html/header/login.html");

	}

	// Datenbankeintrag hinzuf�gen
	private void persist(UserBean userBean) throws ServletException {
		// Name der Spalte, die automatisch generiert wird
		String[] generatedKeys = new String[] { "id" };

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"INSERT INTO user (passwort,mail,vorname,nachname,geburtstag) VALUES (?,?,?,?,?)",
						generatedKeys)) {

			pstmt.setString(1, userBean.getPasswort());
			pstmt.setString(2, userBean.getMail());
			pstmt.setString(3, userBean.getVorname());
			pstmt.setString(4, userBean.getNachname());
			pstmt.setDate(5, userBean.getGeburtstag());
			pstmt.executeUpdate();

			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				while (rs.next()) {
					userBean.setId(rs.getLong(1));
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// AJAX-Abfrage der E-Mail
	private boolean checkMail(String mail) throws ServletException {
		boolean status = false;
		try (Connection con = ds.getConnection();
				PreparedStatement ps = con.prepareStatement("SELECT mail FROM user WHERE mail=?")) {

			ps.setString(1, mail);
			ResultSet rs = ps.executeQuery();
			status = rs.next();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		return status;
	}

}
