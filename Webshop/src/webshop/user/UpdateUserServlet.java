/*
 * Verantwortlich: Felix Gr�hlich
 */
package webshop.user;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
/**
 * Servlet implementation class UpdateUserServlet
 */
@WebServlet("/UpdateUserServlet")
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

    
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		Long alteID = Long.valueOf(request.getParameter("alteID"));
		
		UserBean userBean = new UserBean();
		userBean.setId(Long.valueOf(request.getParameter("neueID")));
		userBean.setMail(request.getParameter("neueMail"));
		userBean.setNachname(request.getParameter("neuerNachname"));
		userBean.setVorname(request.getParameter("neuerVorname"));
		userBean.setPasswort(request.getParameter("neuesPasswort"));
		userBean.setAdmin(Boolean.valueOf(request.getParameter("neueAdmin")));
	
	update(userBean, alteID);

	request.setAttribute("id", alteID);
	
	final RequestDispatcher dispatcher = request.getRequestDispatcher("ReadAllUserServlet");
	dispatcher.forward(request, response);
	
	}
	
	private void update(UserBean userBean, Long alteID) throws ServletException{
	
		try(Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE user SET id = ?, nachname = ?, vorname = ?, mail = ?, passwort = ?, admin = ? WHERE id = ?")){
			
			pstmt.setLong(1, userBean.getId());
			pstmt.setString(2, userBean.getNachname());
			pstmt.setString(3, userBean.getVorname());
			pstmt.setString(4, userBean.getMail());
			pstmt.setString(5, userBean.getPasswort());
			pstmt.setBoolean(6, userBean.getAdmin());
			pstmt.setLong(7, alteID);
			pstmt.executeUpdate();
			
		}catch(Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
