/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class AuftragBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long auftragsnummer;
	private Long userID;
	private Date datum;
	private BigDecimal rechnungssumme;
	
	public Long getAuftragsnummer() {
		return auftragsnummer;
	}
	public void setAuftragsnummer(Long auftragsnummer) {
		this.auftragsnummer = auftragsnummer;
	}
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public BigDecimal getRechnungssumme() {
		return rechnungssumme;
	}
	public void setRechnungssumme(BigDecimal rechnungssumme) {
		this.rechnungssumme = rechnungssumme;
	}
}
