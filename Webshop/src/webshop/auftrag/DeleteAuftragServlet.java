/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/DeleteAuftragServlet")

public class DeleteAuftragServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		// L�schen nicht �ber Link m�glich, nur �ber Icons auf CRUD Seite
		if (request.getHeader("referer") == null || !request.getHeader("referer").contains("AuftragServlet")) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response,
					"Sie sind nicht berechtigt diese Funktion aufzurufen!");
			return;
		}
		

		// Zu l�schende Auftragsnummer
		Long auftragsnummer = Long.valueOf(request.getParameter("auftragsnummer"));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			delete(auftragsnummer);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Auftrag-�bersicht
		response.sendRedirect("ReadAllAuftragServlet");

	}

	// Datenbankzugriff
	private void delete(Long auftragsnummer) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("DELETE FROM auftrag WHERE auftragsnummer = ?")) {
			pstmt.setLong(1, auftragsnummer);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
