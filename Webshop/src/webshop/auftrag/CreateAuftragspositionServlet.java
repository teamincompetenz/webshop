/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/CreateAuftragspositionServlet")
public class CreateAuftragspositionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Inhalten f�llen
		AuftragspositionBean apb = new AuftragspositionBean();
		apb.setPositionsID(Long.valueOf(request.getParameter("positionsID")));
		apb.setAuftragsnummer(Long.valueOf(request.getParameter("auftragsnummer")));
		apb.setProduktID(Long.valueOf(request.getParameter("produktID")));
		apb.setAnzahl(Integer.valueOf(request.getParameter("anzahl")));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			persist(apb);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Auftrags-�bersicht
		response.sendRedirect("ReadAllAuftragspositionServlet?auftragsnummer=" + (apb.getAuftragsnummer().toString()));
	}

	// Datenbankeintrag hinzuf�gen
	private void persist(AuftragspositionBean auftragspositionBean) throws ServletException {

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"INSERT INTO auftragsposition (positionsID, auftragsnummer, produktID, anzahl) VALUES (?,?,?,?);")) {

			pstmt.setLong(1, auftragspositionBean.getPositionsID());
			pstmt.setLong(2, auftragspositionBean.getAuftragsnummer());
			pstmt.setLong(3, auftragspositionBean.getProduktID());
			pstmt.setInt(4, auftragspositionBean.getAnzahl());
			pstmt.executeUpdate();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
}
