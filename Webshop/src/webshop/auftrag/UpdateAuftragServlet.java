/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/UpdateAuftragServlet")

public class UpdateAuftragServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Inhalten f�llen
		Long alteAuftragsnummer = Long.valueOf(request.getParameter("alteAuftragsnummer"));
		AuftragBean auftragBean = new AuftragBean();
		auftragBean.setAuftragsnummer(Long.valueOf(request.getParameter("neueAuftragsnummer")));
		auftragBean.setUserID(Long.valueOf(request.getParameter("userID")));
		auftragBean.setDatum(Date.valueOf(request.getParameter("datum")));
		auftragBean.setRechnungssumme(BigDecimal.valueOf(Double.valueOf(request.getParameter("rechnungssumme"))));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			update(auftragBean, alteAuftragsnummer);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Auftrag-�bersicht
		response.sendRedirect("ReadAllAuftragServlet");
	}

	// Datenbankeintrag updaten
	private void update(AuftragBean auftragBean, Long alteID) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"UPDATE auftrag SET auftragsnummer = ?, userID = ?, datum = ?, rechnungssumme = ? WHERE auftragsnummer = ?")) {
			pstmt.setLong(1, auftragBean.getAuftragsnummer());
			pstmt.setLong(2, auftragBean.getUserID());
			pstmt.setDate(3, auftragBean.getDatum());
			pstmt.setBigDecimal(4, auftragBean.getRechnungssumme());
			pstmt.setLong(5, alteID);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
