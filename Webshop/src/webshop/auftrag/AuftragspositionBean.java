/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.Serializable;

public class AuftragspositionBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long positionsID;
	private Long auftragsnummer;
	private Long produktID;
	private int anzahl;

	public Long getPositionsID() {
		return positionsID;
	}
	public void setPositionsID(Long positionsID) {
		this.positionsID = positionsID;
	}
	public Long getAuftragsnummer() {
		return auftragsnummer;
	}
	public void setAuftragsnummer(Long auftragsnummer) {
		this.auftragsnummer = auftragsnummer;
	}
	public Long getProduktID() {
		return produktID;
	}
	public void setProduktID(Long produktID) {
		this.produktID = produktID;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
}