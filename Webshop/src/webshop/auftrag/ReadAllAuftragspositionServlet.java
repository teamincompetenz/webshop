/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/ReadAllAuftragspositionServlet")

public class ReadAllAuftragspositionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Liste mit DB-Inhalt f�llen
		Long auftragsnummer = Long.valueOf(request.getParameter("auftragsnummer"));
		List<AuftragspositionBean> apbList = read(auftragsnummer);

		// Scope "Request"
		request.setAttribute("auftragspositionliste", apbList);
		request.setAttribute("auftragsnummer", auftragsnummer);

		// Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/auftrag/crudAuftragspositionen.jsp");
		dispatcher.forward(request, response);
	}

	// Datenbankzugriff
	private List<AuftragspositionBean> read(Long auftragsnummer) throws ServletException {
		List<AuftragspositionBean> apbList = new ArrayList<AuftragspositionBean>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("SELECT * FROM auftragsposition WHERE auftragsnummer = ?")) {
			pstmt.setLong(1, auftragsnummer);
			ResultSet rs = pstmt.executeQuery();

			// List mit ResultSet Ergebnissen f�llen
			while (rs.next()) {
				AuftragspositionBean apb = new AuftragspositionBean();
				apb.setPositionsID(rs.getLong("positionsID"));
				apb.setAuftragsnummer(rs.getLong("auftragsnummer"));
				apb.setProduktID(rs.getLong("produktID"));
				apb.setAnzahl(rs.getInt("anzahl"));
				apbList.add(apb);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return apbList;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
