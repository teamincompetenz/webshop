/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/CreateAuftragServlet")

public class CreateAuftragServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Inhalten f�llen
		AuftragBean auftragBean = new AuftragBean();
		auftragBean.setRechnungssumme(BigDecimal.valueOf(Double.valueOf(request.getParameter("rechnungssumme"))));
		auftragBean.setUserID(Long.valueOf(request.getParameter("userID")));
		auftragBean.setDatum(Date.valueOf(request.getParameter("datum")));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			persist(auftragBean);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response,servex.getMessage());
			return;
		}

		// Zur�ck auf die Auftrags-�bersicht
		response.sendRedirect("ReadAllAuftragServlet");
	}

	// Datenbankeintrag hinzuf�gen
	private void persist(AuftragBean auftragBean) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"INSERT INTO auftrag (userID, datum, rechnungssumme) VALUES (?,?,?);")) {

			pstmt.setLong(1, auftragBean.getUserID());
			pstmt.setDate(2, auftragBean.getDatum());
			pstmt.setBigDecimal(3, auftragBean.getRechnungssumme());
			pstmt.executeUpdate();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
}
