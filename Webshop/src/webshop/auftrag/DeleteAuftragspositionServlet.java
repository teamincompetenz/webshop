/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/DeleteAuftragspositionServlet")

public class DeleteAuftragspositionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// L�schen nicht �ber Link m�glich, nur �ber Icons auf CRUD Seite
		if (request.getHeader("referer") == null
				|| !request.getHeader("referer").contains("AuftragspositionServlet")) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response,
					"Sie sind nicht berechtigt diese Funktion aufzurufen!");
			return;
		}

		// Zu l�schende PositionsID
		Long positionsID = Long.valueOf(request.getParameter("positionsID"));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			delete(positionsID);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Auftrag-�bersicht
		response.sendRedirect(
				"ReadAllAuftragspositionServlet?auftragsnummer=" + request.getParameter("auftragsnummer"));

	}

	// Datenbankzugriff
	private void delete(Long positionsID) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("DELETE FROM auftragsposition WHERE positionsID = ?")) {
			pstmt.setLong(1, positionsID);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
