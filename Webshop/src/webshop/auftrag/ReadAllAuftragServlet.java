/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/ReadAllAuftragServlet")

public class ReadAllAuftragServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Liste mit DB-Inhalt f�llen
		List<AuftragBean> abList = read();

		// Scope "Request"
		request.setAttribute("auftragliste", abList);

		// Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/auftrag/crudAuftrag.jsp");
		dispatcher.forward(request, response);
	}

	// Datenbankzugriff
	private List<AuftragBean> read() throws ServletException {
		List<AuftragBean> abList = new ArrayList<AuftragBean>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM auftrag");
				ResultSet rs = pstmt.executeQuery()) {

			// List mit ResultSet Ergebnissen f�llen
			while (rs.next()) {
				AuftragBean ab = new AuftragBean();
				ab.setAuftragsnummer(rs.getLong("auftragsnummer"));
				ab.setUserID(rs.getLong("userID"));
				ab.setDatum(rs.getDate("datum"));
				ab.setRechnungssumme(rs.getBigDecimal("rechnungssumme"));
				abList.add(ab);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return abList;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
