/*
 * Verantwortlich: Jan Hermann
 */

package webshop.auftrag;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/UpdateAuftragspositionServlet")

public class UpdateAuftragspositionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		Long altePositionsID = Long.valueOf(request.getParameter("altePositionsID"));

		AuftragspositionBean auftragspositionBean = new AuftragspositionBean();
		auftragspositionBean.setAuftragsnummer(Long.valueOf(request.getParameter("auftragsnummer")));
		auftragspositionBean.setPositionsID(Long.valueOf(request.getParameter("neuePositionsID")));
		auftragspositionBean.setProduktID(Long.valueOf(request.getParameter("produktID")));
		auftragspositionBean.setAnzahl(Integer.valueOf(request.getParameter("anzahl")));

		// Datenbankzugriff und Fehlerbehandlung
		try {
			update(auftragspositionBean, altePositionsID);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Auftrag-�bersicht
		response.sendRedirect("ReadAllAuftragspositionServlet?auftragsnummer="
				+ (auftragspositionBean.getAuftragsnummer().toString()));

	}

	// Datenbankeintrag updaten
	private void update(AuftragspositionBean auftragspositionBean, Long altePositionsID) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"UPDATE auftragsposition SET positionsID = ?, produktID = ?, anzahl = ? WHERE auftragsnummer = ? AND positionsID = ?")) {
			pstmt.setLong(1, auftragspositionBean.getPositionsID());
			pstmt.setLong(2, auftragspositionBean.getProduktID());
			pstmt.setInt(3, auftragspositionBean.getAnzahl());
			pstmt.setLong(4, auftragspositionBean.getAuftragsnummer());
			pstmt.setLong(5, altePositionsID);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
