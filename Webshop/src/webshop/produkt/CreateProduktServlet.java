/*
 * Verantwortlich: Jan Hermann | Felix Gr�hlich (Zeile 47 bis 53)
 */

package webshop.produkt;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/CreateProduktServlet")
@MultipartConfig(maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5
		* 5, location = "/tmp", fileSizeThreshold = 1024 * 1024 * 5)

public class CreateProduktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Bean mit Form-Inhalt f�llen
		ProduktBean produktBean = new ProduktBean();
		produktBean.setBezeichnung(request.getParameter("bezeichnung"));
		produktBean.setKategorie(request.getParameter("kategorie"));
		produktBean.setPreis(BigDecimal.valueOf(Double.valueOf(request.getParameter("preis"))));
		produktBean.setFarbe(request.getParameter("farbe"));
		produktBean.setOriginalpreis(BigDecimal.valueOf(Double.valueOf(request.getParameter("originalpreis"))));
		
		// Verantwortlicher: Felix Gr�hlich
		produktBean.setBeschreibung(request.getParameter("beschreibung"));
		produktBean.setHoehe(Integer.valueOf(request.getParameter("hoehe")));
		produktBean.setBreite(Integer.valueOf(request.getParameter("breite")));
		produktBean.setTiefe(Integer.valueOf(request.getParameter("tiefe")));
		produktBean.setGewicht(BigDecimal.valueOf(Double.valueOf(request.getParameter("gewicht"))));
		produktBean.setSale(Boolean.valueOf(request.getParameter("sale")));
		produktBean.setLagerbestand(Integer.valueOf(request.getParameter("lagerbestand")));

		// Filebehandlung
		Part filepart = request.getPart("image");
		produktBean.setFilename(filepart.getSubmittedFileName());

		// Datenbankzugriff und Fehlerbehandlung
		try {
			persist(produktBean, filepart);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response,servex.getMessage());
			return;
		}

		// Zur�ck auf die Produkt-�bersicht
		response.sendRedirect("ReadAllProduktServlet?reload=true");

	}
	
	// Datenbankeintrag hinzuf�gen
	private void persist(ProduktBean produktBean, Part filepart) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"INSERT INTO produkt (bezeichnung,preis,kategorie,farbe,beschreibung,hoehe,breite,tiefe,gewicht,sale,originalpreis,lagerbestand,filename,file) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);")) {

			pstmt.setString(1, produktBean.getBezeichnung());
			pstmt.setBigDecimal(2, produktBean.getPreis());
			pstmt.setString(3, produktBean.getKategorie());
			pstmt.setString(4, produktBean.getFarbe());
			pstmt.setString(5, produktBean.getBeschreibung());
			pstmt.setLong(6, produktBean.getHoehe());
			pstmt.setLong(7, produktBean.getBreite());
			pstmt.setLong(8, produktBean.getTiefe());
			pstmt.setBigDecimal(9, produktBean.getGewicht());
			pstmt.setBoolean(10, produktBean.getSale());
			pstmt.setBigDecimal(11, produktBean.getOriginalpreis());
			pstmt.setLong(12, produktBean.getLagerbestand());
			pstmt.setString(13, produktBean.getFilename());
			pstmt.setBinaryStream(14, filepart.getInputStream());
			pstmt.executeUpdate();

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
	
}
