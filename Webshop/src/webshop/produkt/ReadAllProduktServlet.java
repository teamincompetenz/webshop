/*
 * Verantwortlich: Jan Hermann
 */

package webshop.produkt;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Datenbankzugriff;
import webshop.allgemein.Fehlerbehandlung;
import webshop.farben.FarbeBean;
import webshop.kategorie.KategorieBean;

@WebServlet("/ReadAllProduktServlet")
public class ReadAllProduktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Listen mit Datenbankinhalt f�llen
		List<ProduktBean> pbList = suche(request.getParameter("suche"));
		List<KategorieBean> kbList = Datenbankzugriff.alleKategorien(ds);
		List<FarbeBean> fbList = Datenbankzugriff.alleFarben(ds);

		// Scope "Request"
		request.setAttribute("produktliste", pbList);
		request.setAttribute("kategorieliste", kbList);
		request.setAttribute("farbeliste", fbList);

		// In Abh�ngig des Referers und des ReloadTags wird die richtige JSP-Datei ausgew�hlt
		if (request.getHeader("referer").contains("ReadAllProduktServlet") && !Boolean.valueOf(request.getParameter("reload"))) {
			final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/produktTable.jsp");
			dispatcher.forward(request, response);
		} else {
			final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/crudProdukt.jsp");
			dispatcher.forward(request, response);
		}
	}

	// Datenbankabfrage mit Filterfunktion f�r Bezeichnung
	private List<ProduktBean> suche(String bezeichnungSuche) throws ServletException {
		bezeichnungSuche = (bezeichnungSuche == null || bezeichnungSuche == "") ? "%" : "%" + bezeichnungSuche + "%";
		List<ProduktBean> pbList = new ArrayList<ProduktBean>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM produkt WHERE bezeichnung LIKE ?")) {

			pstmt.setString(1, bezeichnungSuche);

			try (ResultSet rs = pstmt.executeQuery()) {
				// List mit ResultSet Ergebnissen f�llen
				while (rs.next()) {
					ProduktBean pb = new ProduktBean();
					pb.setId(rs.getLong("id"));
					pb.setBezeichnung(rs.getString("bezeichnung"));
					pb.setPreis(rs.getBigDecimal("preis"));
					pb.setHoehe(rs.getInt("hoehe"));
					pb.setBreite(rs.getInt("breite"));
					pb.setTiefe(rs.getInt("tiefe"));
					pb.setGewicht(rs.getBigDecimal("gewicht"));
					pb.setBeschreibung(rs.getString("beschreibung"));
					pb.setSale(rs.getBoolean("sale"));
					pb.setOriginalpreis(rs.getBigDecimal("originalpreis"));
					pb.setLagerbestand(rs.getInt("lagerbestand"));
					pb.setFilename(rs.getString("filename"));
					pb.setKategorie(rs.getString("kategorie"));
					pb.setFarbe(rs.getString("farbe"));
					pb.setImage(rs.getBytes("file"));
					pbList.add(pb);
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		return pbList;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
