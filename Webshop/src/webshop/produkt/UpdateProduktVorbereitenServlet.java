/*
 * Verantwortlich: Jan Hermann
 */

package webshop.produkt;

import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Datenbankzugriff;
import webshop.allgemein.Fehlerbehandlung;
import webshop.farben.FarbeBean;
import webshop.kategorie.KategorieBean;

@WebServlet("/UpdateProduktVorbereitenServlet")

public class UpdateProduktVorbereitenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// Produkt mit angegebener ID in Bean speichern
		Long id = Long.valueOf(request.getParameter("id"));
		ProduktBean pb = Datenbankzugriff.readProdukt(ds, id);
		
		// Listen mit Datenbankinhalt f�llen
		List<KategorieBean> kbList = Datenbankzugriff.alleKategorien(ds);
		List<FarbeBean> fbList  = Datenbankzugriff.alleFarben(ds);

		// Scope "Request"
		request.setAttribute("produktBean", pb);
		request.setAttribute("kategorieliste", kbList);
		request.setAttribute("farbeliste", fbList);
		
		// Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/updateProdukt.jsp");
		dispatcher.forward(request, response);

	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}
}
