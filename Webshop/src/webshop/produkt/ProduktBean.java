/*
 * Verantwortlich: Jan Hermann | Felix Gr�hlich (Zeile 23 bis 31)
 */

package webshop.produkt;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProduktBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String bezeichnung;
	private BigDecimal preis;
	private String filename;
	private String kategorie;
	private String farbe;
	private BigDecimal originalpreis;
	private byte[] image;
	
	// Verantwortlicher: Felix Gr�hlich
	private int hoehe;
	private int breite;
	private int tiefe;
	private BigDecimal gewicht;
	private String beschreibung;
	private Boolean sale;
	private int lagerbestand;
	private Long anzahl;
	private BigDecimal summenpreis;

	
	
	
	public BigDecimal getSummenpreis() {
		return summenpreis;
	}
	public void setSummenpreis(BigDecimal summenpreis) {
		this.summenpreis = summenpreis;
	}
	public Long getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(Long anzahl) {
		this.anzahl = anzahl;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPreis() {
		return preis;
	}
	public void setPreis(BigDecimal preis) {
		this.preis = preis;
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getKategorie() {
		return kategorie;
	}
	public void setKategorie(String kategorie) {
		this.kategorie = kategorie;
	}	
	
	public String getFarbe() {
		return farbe;
	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}
	
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getHoehe() {
		return hoehe;
	}
	public void setHoehe(int hoehe) {
		this.hoehe = hoehe;
	}
	public int getBreite() {
		return breite;
	}
	public void setBreite(int breite) {
		this.breite = breite;
	}
	public int getTiefe() {
		return tiefe;
	}
	public void setTiefe(int tiefe) {
		this.tiefe = tiefe;
	}
	public BigDecimal getGewicht() {
		return gewicht;
	}
	public void setGewicht(BigDecimal gewicht) {
		this.gewicht = gewicht;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	public Boolean getSale() {
		return sale;
	}
	public void setSale(Boolean sale) {
		this.sale = sale;
	}
	public int getLagerbestand() {
		return lagerbestand;
	}
	public void setLagerbestand(int lagerbestand) {
		this.lagerbestand = lagerbestand;
	}
	public BigDecimal getOriginalpreis() {
		return originalpreis;
	}
	public void setOriginalpreis(BigDecimal originalpreis) {
		this.originalpreis = originalpreis;
	}
	
}
