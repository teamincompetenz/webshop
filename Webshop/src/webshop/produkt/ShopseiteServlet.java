/*
 * Verantwortlich: Daniel Fina
 */

package webshop.produkt;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import webshop.allgemein.Datenbankzugriff;
import webshop.allgemein.Fehlerbehandlung;
import webshop.farben.FarbeBean;
import webshop.kategorie.KategorieBean;

@WebServlet("/ShopseiteServlet")
public class ShopseiteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String kategorie = request.getParameter("kategorie");

		// Listen mit Inhalt f�llen
		List<ProduktBean> pbList = read(kategorie);
		List<KategorieBean> kbList = Datenbankzugriff.alleKategorien(ds);
		List<FarbeBean> fbList = Datenbankzugriff.alleFarben(ds);

		// Scope "Request"
		request.setAttribute("produktliste", pbList);
		request.setAttribute("kategorieliste", kbList);
		request.setAttribute("farbeliste", fbList);

		// Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/shopseite.jsp");
		dispatcher.forward(request, response);

	}

	// Datenbankabfrage der Produkte und Erstellung einer Liste an Produkten
	private List<ProduktBean> read(String kategorie) throws ServletException {
		List<ProduktBean> pbList = new ArrayList<ProduktBean>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM produkt");
		if (kategorie != null) {
			sb.append(" where kategorie = ?");
		}

		try (Connection con = ds.getConnection(); PreparedStatement pstmt = con.prepareStatement(sb.toString())) {
			if (kategorie != null) {
				pstmt.setString(1, kategorie);
			}
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				ProduktBean pb = new ProduktBean();
				pb.setId(rs.getLong("id"));
				pb.setBezeichnung(rs.getString("bezeichnung"));
				pb.setPreis(rs.getBigDecimal("preis"));
				pb.setFilename(rs.getString("filename"));
				pb.setKategorie(rs.getString("kategorie"));
				pb.setFarbe(rs.getString("farbe"));
				pb.setImage(rs.getBytes("file"));
				pb.setSale(rs.getBoolean("sale"));
				pb.setOriginalpreis(rs.getBigDecimal("originalpreis"));
				pbList.add(pb);
			}

		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return pbList;
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
