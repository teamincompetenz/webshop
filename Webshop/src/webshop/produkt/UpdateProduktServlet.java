/*
 * Verantwortlich: Jan Hermann
 */

package webshop.produkt;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import webshop.allgemein.Fehlerbehandlung;

@WebServlet("/UpdateProduktServlet")
@MultipartConfig(maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5
		* 5, location = "/tmp", fileSizeThreshold = 1024 * 1024 * 5)

public class UpdateProduktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		Long alteID = Long.valueOf(request.getParameter("alteID"));

		// Bean mit Form-Inhalt f�llen
		ProduktBean produktBean = new ProduktBean();
		produktBean.setId(Long.valueOf(request.getParameter("neueID")));
		produktBean.setBezeichnung(request.getParameter("neueBezeichnung"));
		produktBean.setKategorie(request.getParameter("kategorie"));
		produktBean.setPreis(BigDecimal.valueOf(Double.valueOf(request.getParameter("neuerPreis"))));
		produktBean.setFarbe(request.getParameter("farbe"));
		produktBean.setBreite(Integer.valueOf(request.getParameter("neueBreite")));
		produktBean.setHoehe(Integer.valueOf(request.getParameter("neueHoehe")));
		produktBean.setTiefe(Integer.valueOf(request.getParameter("neueTiefe")));
		produktBean.setGewicht(BigDecimal.valueOf(Double.valueOf(request.getParameter("neuesGewicht"))));
		produktBean.setBeschreibung(request.getParameter("neueBeschreibung"));
		produktBean.setSale(Boolean.valueOf(request.getParameter("sale")));
		produktBean.setOriginalpreis(BigDecimal.valueOf(Double.valueOf(request.getParameter("neuerOriginalpreis"))));
		produktBean.setLagerbestand(Integer.valueOf(request.getParameter("neuerLagerbestand")));
		produktBean.setFilename(request.getParameter("filename"));

		// Datenbankzugriff und Fehlerbehandlung
		try {

			// Neues Bild einf�gen, falls vorhanden
			Part filepart = request.getPart("neuesBild");
			if (filepart.getSize() > 0) {
				updateBild(alteID, filepart);
			}

			update(produktBean, alteID, filepart);
		} catch (ServletException servex) {
			Fehlerbehandlung.exeptionWeiterleitung(request, response, servex.getMessage());
			return;
		}

		// Zur�ck auf die Kategorie-�bersicht
		response.sendRedirect("ReadAllProduktServlet");

	}

	// Bild in Datenbank aktualisieren
	private void updateBild(Long id, Part filepart) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE produkt SET file = ? WHERE id = ?")) {

			pstmt.setBinaryStream(1, filepart.getInputStream());
			pstmt.setLong(2, id);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Datenbankeintrag updaten
	private void update(ProduktBean produktBean, Long alteID, Part filepart) throws ServletException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("UPDATE produkt SET id = ?, bezeichnung = ?, preis = ?, hoehe = ?, breite = ?"
								+ ", tiefe = ?, gewicht = ?, beschreibung = ?, lagerbestand = ?, sale = ?, originalpreis = ?, farbe = ?, kategorie = ?, filename = ? WHERE id = ?")) {

			pstmt.setLong(1, produktBean.getId());
			pstmt.setString(2, produktBean.getBezeichnung());
			pstmt.setBigDecimal(3, produktBean.getPreis());
			pstmt.setInt(4, produktBean.getHoehe());
			pstmt.setInt(5, produktBean.getBreite());
			pstmt.setInt(6, produktBean.getTiefe());
			pstmt.setBigDecimal(7, produktBean.getGewicht());
			pstmt.setString(8, produktBean.getBeschreibung());
			pstmt.setInt(9, produktBean.getLagerbestand());
			pstmt.setBoolean(10, produktBean.getSale());
			pstmt.setBigDecimal(11, produktBean.getOriginalpreis());
			pstmt.setString(12, produktBean.getFarbe());
			pstmt.setString(13, produktBean.getKategorie());
			pstmt.setString(14, produktBean.getFilename());
			pstmt.setLong(15, alteID);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	// Methode leitet auf Fehlerseite weiter
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Fehlerbehandlung.exeptionWeiterleitung(request, response,
				"Das Servlet ist &uuml;ber diese Methode nicht aufrufbar");
	}

}
