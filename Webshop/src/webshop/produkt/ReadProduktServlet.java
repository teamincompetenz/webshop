/*
 * Verantwortlich: Felix Gr�hlich
 */

package webshop.produkt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import webshop.allgemein.Datenbankzugriff;

/**
 * Servlet implementation class ProduktseiteServlet
 */
@WebServlet("/ReadProduktServlet")
public class ReadProduktServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReadProduktServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		Long id = Long.valueOf(request.getParameter("produktID"));

		// Bean erstellen und mit DB-Inhalt f�llen
		ProduktBean produktBean = Datenbankzugriff.readProdukt(ds, id);

		Boolean reserviertbool = false;

		List<ProduktBean> session = new ArrayList<ProduktBean>();

		ServletContext sc = this.getServletContext();

		List<HttpSession> listener = (List<HttpSession>) sc.getAttribute("sessionlist");

		for (HttpSession se : listener) {

			if (se.getAttribute("warenkorb") != null) {
				session.addAll((List<ProduktBean>) se.getAttribute("warenkorb"));
			}
		}
		int reserviert = 0;

		for (ProduktBean pb : session) {
			if (pb.getId().equals(produktBean.getId())) {

				if (pb.getAnzahl() != null) {
					
					reserviert += pb.getAnzahl().intValue();
				}else {
					reserviert = 0;
				}

				if (pb.getAnzahl().intValue() >= produktBean.getLagerbestand()) {
					reserviertbool = true;
				}

			}
		}

		// Scope "Request"
		request.setAttribute("produktBean", produktBean);
		request.setAttribute("reserviert", reserviert);
		request.setAttribute("reserviertbool", reserviertbool);
		request.setAttribute("user", request.getSession().getAttribute("userBean"));

		// Weiterleiten an JSP
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/produkt/produktSeite.jsp");
		dispatcher.forward(request, response);
	}
}
