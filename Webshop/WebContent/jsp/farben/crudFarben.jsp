<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Farben bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="createFarbeForm" method="post"
					action="../../CreateFarbeServlet" accept-charset="utf-8">

					<%-- Farbe erstellen --%>
					<div class="bearbeitung">
						<h2>Neue Farbe anlegen:</h2>
						<b></b> <label for="Farbe">Bezeichnung:</label><br> <input
							type="text" name="bezeichnung" id="bezeichnung"
							placeholder="Hier eingeben..." required> <br> <br>

						<div class="buttonContainer">
							<button name="submit" type="submit" id="submit">Absenden</button>
							<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
						</div>
						<p>
							<a class="zurueck" href="../allgemein/admin.jsp?">&laquo;
								Zur&uuml;ck zur Adminseite</a>
						</p>
					</div>

				</form>

				<div id="crudTable">
					<h2>Alle Farben:</h2>
					<table>
						<tr>
							<th>Id</th>
							<th>Bezeichnung</th>
							<th colspan=2>Aktionen</th>
						</tr>

						<%-- Alle Farbe ausgeben --%>
						<c:forEach items="${farbeliste}" var="farbeBean">
							<tr>
								<td>${farbeBean.id}</td>
								<td class="farbeBezeichnung">${farbeBean.bezeichnung}</td>

								<%-- Farbe L&ouml;schen --%>
								<td><a href="../../DeleteFarbeServlet?id=${farbeBean.id}">
										<img alt="FarbeL&ouml;schen" src="../../img/loeschen.png">
								</a></td>

								<%-- Farbe Updaten --%>
								<td><a
									href="updateFarbe.jsp?alteID=${farbeBean.id}&alteBezeichnung=${farbeBean.bezeichnung}"><img
										alt="FarbeBearbeiten" src="../../img/bearbeiten.png"></a></td>
							</tr>
						</c:forEach>
					</table>
					<br>
				</div>

			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>