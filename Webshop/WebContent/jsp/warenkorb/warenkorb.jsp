<!-- Verantwortlich: Felix Gröhlich -->
<%@ page language="java" contentType="text/html;UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Warenkorb</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<link href="../../css/user/warenkorb.css" rel="stylesheet">
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/cookie.js"></script>

</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>


	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%>
	</nav>

	<main>
		<div id="flex">
			<div class="checkout">
				<c:choose>
					<c:when test="${summe > 0}">
						<p>Summe: ${summe} &euro;</p>
						<button type="submit" id="checkout"
							onclick="document.location = 'checkout.jsp'">Zum
							Checkout</button>
						<input type="hidden" id="summe" value="${summe}">
					</c:when>
					<c:otherwise>
						<p>Sie haben noch keine Produkte im Warenkorb!</p>
						<a href="../../ShopseiteServlet">Zur&uuml;ck zum Shop</a>
					</c:otherwise>

				</c:choose>


			</div>
			<div id="warenkorbContainer">
				<form id="warenkorb" method="post"
					action="../../ListWarenkorbServlet" enctype=multipart/form-data
					accept-charset="utf-8">
					<div class="warenkorb">
						<c:forEach items="${warenkorb}" var="produktBean">

							<ul>
								<li class="imgContainer">
									<div id="imgContainer">
										<img class="img"
											src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
											alt="${produktBean.bezeichnung}">
									</div>
									<hr>
								</li>
								<li>
								<li><div id="bezeichnung"><h4>${produktBean.bezeichnung}</h4></div></li>
								<li>Gesamtpreis: ${produktBean.summenpreis}</li>
								<li>Einzelpreis: ${produktBean.preis}&euro;</li>
								<li>Anzahl: ${produktBean.anzahl} Stk <input type="hidden"
									id="produktID" name="produktID" value="${produktBean.id}"></li>
								<li><a
									href="../../DeleteCartProduktServlet?id=${produktBean.id}"><img
										class="deleteIcon" alt="ProduktL&ouml;schen"
										src="../../img/loeschen.png"></a></li>
							</ul>

						</c:forEach>
					</div>
				</form>
			</div>

		</div>
	</main>
	<footer>
		<%@ include file="../../jspf/footerFragment.jspf"%>
	</footer>
</body>
</html>