<!-- Verantwortlicher: Felix Gröhlich -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI }" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Checkout</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<link href="../../css/user/checkout.css" rel="stylesheet">
<link href="../../css/user/bestellung.css" rel="stylesheet">
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/cookie.js"></script>

<title>Bestaetigen</title>
</head>

<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>
	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%>
	</nav>
	<main>
		<div class="bestelldaten">
			<fieldset>
				<legend>Ihre Bestellung: </legend>
				<ul>
					<li><label>Vorname:</label> ${userBean.vorname}
					<li><label>Nachname:</label> ${userBean.nachname}
					<li><label>Lieferadresse:</label> ${adresse}
					<li><label>Ware:</label> 
					<c:forEach items="${warenkorb}" var="produktBean">

							<p>${produktBean.bezeichnung}, ${produktBean.anzahl} St&uuml;ck</p>
						</c:forEach>
					<li><label>Lieferart:</label> ${lieferung}
					<li><label>Gesamtkosten zzgl. Lieferkosten:</label> ${lieferSumme} &euro;
					<li><label>Zahlungsart:</label> ${zahlung}
				</ul>
				<a href="../../ListWarenkorbServlet">Zur&uuml;ck</a> <a
					href="../../BezahlenServlet">Bezahlen</a>
			</fieldset>
		</div>
	</main>
</body>
</html>