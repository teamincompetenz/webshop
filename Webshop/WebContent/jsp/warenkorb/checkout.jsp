<!-- Verantwortlich: Daniel Fina | Felix Gröhlich (Zeilen 61-79) -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI }" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Checkout</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<link href="../../css/user/checkout.css" rel="stylesheet">
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/cookie.js"></script>
<title>Checkout</title>
</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>
	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%></nav>
	<main>
		<form id="checkoutForm" method="post"
			action="../../BezahlungBestaetigenServlet">
			<fieldset>
				<legend>Checkout</legend>
				<div class="checkoutInfo">
					<label class="daten">Ihre Daten:</label><br> 
					<label for="vorname">Vorname:</label><br> 
					<input type="text" name="vorname" id="vorname" placeholder="Ihr Vorname..." value="${userBean.vorname}" disabled required><br> 
					<label for="nachname">Nachname:</label><br> 
					<input type="text" name="nachname" id="nachname" placeholder="Ihr Nachname..." value="${userBean.nachname}" disabled required><br> 
					<label for="ort">Ort:</label><br> 
					<input type="text" name="ort" id="ort" placeholder="Ihren Ort eingeben.." pattern="([a-zA-Z]){2,}" required><br>
					<label for="plz">PLZ:</label><br> <input type="text"
						name="plz" id="plz" placeholder="Ihre PLZ eingeben.." pattern="[0-9]{5}" required><br>

					<label for="strasse">Stra&szlig;e:</label><br> <input
						type="text" name="strasse" id="strasse"
						placeholder="Ihre Stra&szlig;e.." required><br>

				</div>
				<div class="checkoutLieferung">
					<label class="lieferung" for="lieferung">Lieferung:</label><br>
					<input class="standard" type="radio" id="standard" name="lieferung"
						value="standard" checked required> <label for="standard">Standard-Lieferung
						45,99&euro;</label><br>
					<p>Lieferung in 7-14 Werktagen</p>
					<input type="radio" id="express" name="lieferung" value="express">
					<label for="express">Express-Lieferung: 79,99&euro;</label><br>
					<p>Lieferung in 2-3 Werktagen</p>

				</div>
				<!-- Verantwortlich: Felix Gröhlich -->
				<div class="zahlungsOptionen">
					<label class="zahlung" for="zahlung">Zahlungsmethode
						wählen:</label> <br> <input type="radio" id="bank" name="zahlung"
						value="Bankeinzugskonto" checked required> <label
						for="bank">Bankeinzugskonto</label><br>
					<p>
						<input type="radio" id="paypal" name="zahlung" value="Paypal">
						<label for="paypal">Paypal</label><br>
					<p>
						<input type="radio" id="kreditkarte" name="zahlung"
							value="Kreditkarte"> <label for="kreditkarte">Kreditkarte</label><br>
					<p>
						<input type="radio" id="rechnung" name="zahlung"
							value="Zahlung auf Rechnung"> <label for="rechnung">Zahlung
							auf Rechnung</label><br>
					<p>
				</div>
				<!-- Ende Felix Gröhlich -->
				<div class="checkoutBezahlung">

					<div id="agbContainer">
						<p class="agbCheckout">
							Hiermit stimme ich den <a
								href="../../html/footer/agb.html" target="_blank">AGBs</a> zu
						</p>
					</div>
					<button class="bezahlButton" id="bezahlButton" type="submit">Weiter</button>
				</div>
			</fieldset>
		</form>
	</main>

	<footer>
		<%@ include file="../../jspf/footerFragment.jspf"%>
	</footer>
</body>
</html>