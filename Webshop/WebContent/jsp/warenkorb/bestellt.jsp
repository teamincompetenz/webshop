<!-- Verantwortlicher: Felix Gröhlich -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI }" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Checkout</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<link href="../../css/user/kontoseite.css" rel="stylesheet">
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/cookie.js"></script>

<title>Bestellt</title>
</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>
	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%></nav>
	<main>

		<h1>Vielen Dank für Ihre Bestellung!</h1>
		<p>Hier können Sie sich Ihre Bestellung ansehen:</p>
		<a class="orderHistory"
			href="../../OrderHistoryServlet?id=${userBean.id}">&raquo; Zur
			Bestell&uuml;bersicht</a>
	</main>


</body>
</html>