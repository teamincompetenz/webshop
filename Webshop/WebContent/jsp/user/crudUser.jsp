<!-- Verantwortlich: Daniel Fina | Felix Gröhlich (71 bis 81) -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI }" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>User bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/ajax/userSearch.js"></script>
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>
			<main>
				<div class="bearbeitung">
					<h2>Benutzer suchen:</h2>
					<form>
						<div class="nachnameContainer">
							<label for="nachname">Nachname:</label><br> <input
								type="text" name="nachname" id="nachname"
								placeholder="Nachname eingeben..."><br> <label
								for="mail">E-Mail:</label><br> <input type="text"
								name="mail" id="mail" placeholder="E-Mail eingeben...">
							<p>
								<a class="zurueck" href="../allgemein/admin.jsp">&laquo;
									Zur&uuml;ck zur Adminseite</a>
							</p>
						</div>
					</form>
				</div>
				<!-- Usertabelle -->
				<div id="crudTable">
					<h2>Alle Benutzer:</h2>
					<table>
						<tr>
							<th>Id</th>
							<th>Vorname</th>
							<th>Nachname</th>
							<th>Mail</th>
							<th>Geburtstag</th>
							<th>Passwort</th>
							<th>Admin</th>
							<th colspan="3">Aktionen</th>
						</tr>

						<c:forEach var="ub" items="${userliste}">
							<tr>
								<td>${ub.id}</td>
								<td>${ub.vorname}</td>
								<td>${ub.nachname}</td>
								<td>${ub.mail}</td>
								<td>${ub.geburtstag}</td>
								<td>${ub.passwort}</td>
								<td class="admin">${ub.admin}</td>


								<%-- User L&ouml;schen --%>
								<td><a href="../../DeleteUserServlet?id=${ub.id}&delete=true"><img
										alt="UserL&ouml;schen" src="../../img/loeschen.png"></a></td>
								<!-- Verantwortlich Felix Gröhlich -->
								<%-- User Updaten --%>
								<td><a
									href="updateUser.jsp?alteID=${ub.id}
				&alteMail=${ub.mail}
				&alterVorname=${ub.vorname}
				&alterNachname=${ub.nachname}
				&altesPasswort=${ub.passwort}
				&alteAdmin=${ub.admin}"><img
										alt="UserBearbeiten" src="../../img/bearbeiten.png"></a></td>
								<!-- Ende Felix Gröhlich -->

								<%-- Order History anzeigen --%>
								<td><a href="../../OrderHistoryServlet?id=${ub.id}"><img
										alt="orderHistoryServlet" src="../../img/anzeigen.png"></a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<br>

			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>