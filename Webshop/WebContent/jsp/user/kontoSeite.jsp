<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Konto</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<link href="../../css/user/kontoseite.css" rel="stylesheet">
<script src="../../js/allgemein/cookie.js"></script>
<script src="../../js/allgemein/header.js"></script>
</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>
	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%>
	</nav>
	<main>
		<div id="datenContainer">
			<h1 class="welcome">
				Herzlich Willkommen, <span class="nameWelcome">${userBean.vorname}!</span>
			</h1>
			<table>
				<tr>
					<th colspan="2">Ihre persönlichen Daten:</th>
				</tr>
				<tr>
					<td>Ihre ID</td>
					<td><span>${userBean.id}</span></td>
				</tr>
				<tr>
					<td>Vorname</td>
					<td><span>${userBean.vorname}</span></td>
				</tr>
				<tr>
					<td>Nachname</td>
					<td><span>${userBean.nachname}</span></td>
				</tr>
				<tr>
					<td>Geburtsdatum</td>
					<td><span>${userBean.geburtstag}</span></td>
				</tr>
				<tr>
					<td>E-Mail-Adresse</td>
					<td><span>${userBean.mail}</span></td>
				</tr>
			</table>
		</div>
		<p id="orderHistoryLink">
			<a class="orderHistory"
				href="../../OrderHistoryServlet?id=${userBean.id}">&raquo; Zur
				Bestell&uuml;bersicht</a>
		</p>
	</main>
	<footer>

		<%@ include file="../../jspf/footerFragment.jspf"%>
	</footer>
</body>
</html>