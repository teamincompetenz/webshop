<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
	uri="http://java.sun.com/jsp/jstl/core"%>
<h2>Alle Benutzer:</h2>
<c:choose>
		<c:when test="${userBean == null || empty userBean}">
		keine Treffer!
		</c:when>
		<c:otherwise>
					<table>
						<tr>
							<th>Id</th>
							<th>Vorname</th>
							<th>Nachname</th>
							<th>Mail</th>
							<th>Passwort</th>
							<th>Admin</th>
							<th colspan="3">Aktionen</th>
						</tr>

						<%-- Alle User ausgeben --%>
						<c:forEach var="ub" items="${userliste}" >
							<tr>
								<td>${ub.id}</td>
								<td>${ub.vorname}</td>
								<td>${ub.nachname}</td>
								<td>${ub.mail}</td>
								<td>${ub.passwort}</td>
								<td class="admin">${ub.admin}</td>


								<%-- User L&ouml;schen --%>
								<td><a href="../../DeleteUserServlet?id=${ub.id}&delete=true"><img
										alt="UserL&ouml;schen" src="../../img/loeschen.png"></a></td>

								<%-- User Updaten --%>
								<td><a
									href="updateUser.jsp?alteID=${ub.id}
				&alteMail=${ub.mail}
				&alterVorname=${ub.vorname}
				&alterNachname=${ub.nachname}
				&altesPasswort=${ub.passwort}
				&alteAdmin=${ub.admin}"><img alt="UserBearbeiten" src="../../img/bearbeiten.png"></a></td>

								<%-- Order History anzeigen --%>
								<td><a href="../../OrderHistoryServlet?id=${ub.id}"><img
										alt="orderHistoryServlet"
										src="../../img/anzeigen.png"></a></td>

							</tr>
						</c:forEach>
					</table>
				</c:otherwise>
			</c:choose>