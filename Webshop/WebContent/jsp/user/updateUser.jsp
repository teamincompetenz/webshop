<!-- Verantwortlich: Felix Gröhlich -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>User Bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/updateStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>

			</header>

			<main>
				<form id="updateUserForm" method="get"
					action="../../UpdateUserServlet" accept-charset="utf-8">

					<div class="bearbeitung">
						<h2>Benutzer bearbeiten:</h2>
						<input type="hidden" name="alteID" id="alteID"
							value="${param.alteID}">

						<table>
							<tr>
								<td><label for="id">Neue ID:</label></td>
								<td><input type="text" name="neueID" id="neueID"
									value="${param.alteID}"></td>
							</tr>
							<tr>
								<td><label for="vorname">Neuer Vorname:</label></td>
								<td><input type="text" name="neuerVorname"
									id="neuerVorname" value="${param.alterVorname}"></td>
							</tr>
							<tr>
								<td><label for="nachname">Neuer Nachname:</label></td>
								<td><input type="text" name="neuerNachname"
									id="neuerNachname" value="${param.alterNachname}"></td>
							</tr>
							<tr>
								<td><label for="mail">Neue Mail:</label></td>
								<td><input type="text" name="neueMail" id="neueMail"
									value="${param.alteMail}"></td>
							</tr>
							<tr>
								<td><label for="passwort">Neues Passwort:</label></td>
								<td><input type="text" name="neuesPasswort"
									id="neuesPasswort" value="${param.altesPasswort}"></td>
							</tr>
							<tr>
								<td><label for="admin">Adminrechte:</label></td>
								<td><input type="checkbox" name="neueAdmin" id="neueAdmin"
									value="true"
									${param.alteAdmin == 'true' ? 'checked="checked"':'unchecked'}>
								</td>
							</tr>
						</table>
						<br>
						<button name="submit" type="submit" id="submit">Absenden</button>
						<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
					</div>
					<div>
						<a class="zurueck" href="../../ReadAllUserServlet">&laquo;
							Zur&uuml;ck zur Benutzer-&Uuml;bersicht</a>
					</div>

				</form>
			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>