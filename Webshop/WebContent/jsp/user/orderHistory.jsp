<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Order History Anzeigen</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
</head>
<body>
	<header>
		<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
	</header>

	<main>
		<div id="crudTable" class="crudTable">
			<h2>Order History:</h2>
			User ID: ${userID} <br> <br>
			<table>
				<tr>
					<th></th>
					<th>Auftragsnummer:</th>
					<th>Rechnungsdatum:</th>
					<th>Rechnungssumme:</th>
					<th>ProduktID</th>
					<th>Bezeichnung</th>
					<th>Anzahl</th>
				</tr>

				<%-- Order History ausgeben --%>
				<c:forEach items="${orderHistoryList}" var="orderHistoryBean">
					<tr>
						<td class="produktBild">
							<div id="imgContainer">
								<img class="img"
									src="../../BildAnzeigenServlet?bildId=${orderHistoryBean.produktID}"
									alt="${orderHistoryBean.bezeichnung}">
							</div>
						</td>
						<td>${orderHistoryBean.auftragsnummer}</td>
						<td>${orderHistoryBean.datum}</td>
						<td>${orderHistoryBean.rechnungssumme}</td>
						<td>${orderHistoryBean.produktID}</td>
						<td class="beschr">${orderHistoryBean.bezeichnung}</td>
						<td>${orderHistoryBean.anzahl}</td>
					</tr>
				</c:forEach>
			</table>
		</div>

		<c:choose>
			<c:when test="${userBean.admin}">
				<p>
					<a class="zurueck" href="../../ReadAllUserServlet?id=${userID}">&laquo;
						Zur&uuml;ck zur Userübersicht</a>
				</p>
			</c:when>
			<c:when test="${not userBean.admin}">
				<p>
					<a class="zurueck" href="../user/kontoSeite.jsp">&laquo;
						Zur&uuml;ck zur Kontoseite</a>
				</p>
			</c:when>
		</c:choose>

	</main>
</body>
</html>