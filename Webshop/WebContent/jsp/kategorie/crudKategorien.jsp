<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kategorien bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="createKategorieForm" method="post"
					action="../../CreateKategorieServlet" accept-charset="utf-8">

					<%-- Kategorie erstellen --%>
					<div class="bearbeitung">
						<h2>Neue Kategorie anlegen:</h2>
						<label for="Kategorie">Bezeichnung:</label><br> <input
							type="text" name="bezeichnung" id="bezeichnung"
							placeholder="Hier eingeben..." required> <br> <br>
						<div class="buttonContainer">
							<button name="submit" type="submit" id="submit">Absenden</button>
							<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
						</div>
						<p>
							<a class="zurueck" href="../allgemein/admin.jsp?">&laquo;
								Zur&uuml;ck zur Adminseite</a>
						</p>
					</div>
				</form>

				<div id="crudTable">
					<h2>Alle Kategorien:</h2>
					<table>
						<tr>
							<th>Id</th>
							<th>Bezeichnung</th>
							<th colspan=2>Aktionen</th>
						</tr>

						<%-- Alle Kategorie ausgeben --%>
						<c:forEach items="${kategorieliste}" var="kategorieBean">
							<tr>
								<td>${kategorieBean.id}</td>
								<td class="kategorieBezeichnung">${kategorieBean.bezeichnung}</td>

								<%-- Kategorie L&ouml;schen --%>
								<td><a
									href="../../DeleteKategorieServlet?id=${kategorieBean.id}">
										<img alt="FarbeL&ouml;schen" src="../../img/loeschen.png">
								</a></td>

								<%-- Kategorie Updaten --%>
								<td><a
									href="updateKategorie.jsp?alteID=${kategorieBean.id}&alteBezeichnung=${kategorieBean.bezeichnung}">
										<img alt="FarbeBearbeiten" src="../../img/bearbeiten.png">
								</a></td>
							</tr>
						</c:forEach>
					</table>
					<br>
				</div>

			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>