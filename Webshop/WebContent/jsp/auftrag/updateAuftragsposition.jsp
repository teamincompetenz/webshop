<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<title>Auftragspositionen bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/updateStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="updateAuftragspositionForm" method="post"
					action="../../UpdateAuftragspositionServlet" accept-charset="utf-8">

					<div class="bearbeitung">
						<h2>Auftragsposition bearbeiten:</h2>
						<input type="hidden" name="altePositionsID" id="altePositionsID"
							value="${param.altePositionsID}">

						<table>
							<tr>
								<td><label for="auftragsnummer">Auftragsnummer: ${param.auftragsnummer}</label></td>
								<td><input type="hidden" name="auftragsnummer" id="auftragsnummer"
									value="${param.auftragsnummer}"></td>
							</tr>
							<tr>
								<td><label for="neuePositionsID">Neue PositionsID:</label></td>
								<td><input type="text" name="neuePositionsID" id="neuePositionsID"
									value="${param.altePositionsID}" required></td>
							</tr>
							<tr>
								<td><label for="produktID">Neue ProduktID:</label></td>
								<td><input type="text" name="produktID" id="produktID"
									value="${param.alteProduktID}" required></td>
							</tr>
							<tr>
								<td><label for="anzahl">Neue Anzahl:</label></td>
								<td><input type="text" name="anzahl" id="anzahl"
									value="${param.alteAnzahl}" pattern="\d+(\.\d{2})?" required></td>
							</tr>
						</table>

						<br>
						<button name="submit" type="submit" id="submit">Absenden</button>
						<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
					</div>

					<div>
							<a class="zurueck" href="../../ReadAllAuftragspositionServlet?auftragsnummer=${param.auftragsnummer}">Zur&uuml;ck zur
								Auftragspositions-&Uuml;bersicht</a>
					</div>
				</form>

			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>