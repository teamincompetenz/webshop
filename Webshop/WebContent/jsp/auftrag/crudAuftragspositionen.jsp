<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<title>Auftragspositionen bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="createAuftragspositionForm" method="post"
					action="../../CreateAuftragspositionServlet" accept-charset="utf-8">

					<%-- Auftrag erstellen --%>
					<div class="bearbeitung">
						<h2>Neue Auftragsposition anlegen:</h2>				
						
						
						<input type="hidden" name="auftragsnummer" id="auftragsnummer" value="${auftragsnummer}"><br> 
							
							<label for="positionsID">Positions ID:</label> 
							<br> <input type="text" name="positionsID" id="positionsID"
							placeholder="Hier eingeben..." required><br> 
							
							<label for="produktID">Produkt ID:</label> 
							<br> <input type="text" name="produktID" id="produktID"
							placeholder="Hier eingeben..." required><br> 	
													
							<label for="anzahl">Anzahl:</label> 
							<br> <input type="text" name="anzahl" id="anzahl"
							placeholder="Hier eingeben..." required><br> 

						<div class="buttonContainer">
							<button name="submit" type="submit" id="submit">Absenden</button>
							<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
						</div>
						<p>
							<a class="zurueck" href="../allgemein/admin.jsp?">&laquo;
								Zur&uuml;ck zur Adminseite</a>
						</p>
					</div>
				</form>

				<div id="crudTable">
					<h2>Alle Auftragspositionen:</h2>
					Auftragsnummer: ${auftragsnummer}
					<table>
						<tr>
							<th>PositionsID</th>
							<th>ProduktID</th>
							<th>Anzahl</th>
							<th colspan=2>Aktionen</th>
						</tr>

						<%-- Alle Auftragspositionen ausgeben --%>
						<c:forEach items="${auftragspositionliste}"
							var="auftragspositionBean">
							<tr>
								<td>${auftragspositionBean.positionsID}</td>
								<td>${auftragspositionBean.produktID}</td>
								<td class="apAnzahl">${auftragspositionBean.anzahl}</td>

								<%-- Auftragsposition L&ouml;schen --%>
								<td><a
									href="../../DeleteAuftragspositionServlet?positionsID=${auftragspositionBean.positionsID}&auftragsnummer=${auftragsnummer}">
										<img alt="AuftragspositionL&ouml;schen"
										src="../../img/loeschen.png">
								</a></td>

								<%-- Auftragsposition Updaten --%>
								<td><a
									href="updateAuftragsposition.jsp?altePositionsID=${auftragspositionBean.positionsID}&alteProduktID=${auftragspositionBean.produktID}&alteAnzahl=${auftragspositionBean.anzahl}&auftragsnummer=${auftragsnummer}">
										<img alt="AuftragspositionBearbeiten"
										src="../../img/bearbeiten.png">
								</a></td>
						</c:forEach>
					</table>
					<br> <a class="zurueck" href="../../ReadAllAuftragServlet?">&laquo;
						Zur&uuml;ck zur Auftrags-&Uuml;bersicht</a>
				</div>

			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>