<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<title>Auftr&auml;ge bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>
			<main>
				<form id="createAuftragForm" method="post"
					action="../../CreateAuftragServlet" accept-charset="utf-8">

					<%-- Auftrag erstellen --%>
					<div class="bearbeitung">
						<h2>Neuen Auftrag anlegen:</h2>
							<label
							for="userID">User ID (Kundennummer):</label> <br> <input
							type="text" name="userID" id="userID"
							placeholder="Hier eingeben..." required><br> <label
							for="datum">Rechnungsdatum:</label><br> <input type="date"
							name="datum" id="datum" placeholder="YYYY-MM-DD" required>
						<br> <label for="rechnungssumme">Rechnungssumme:</label><br>
						<input type="text" name="rechnungssumme" id="rechnungssumme"
							placeholder="Betrag eingeben 0.00" pattern="\d+(\.\d{2})?"
							required><br> <br>
					<div class="buttonContainer">
						<button name="submit" type="submit" id="submit">Absenden</button>
						<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
					</div>
					<p><a class="zurueck" href="../allgemein/admin.jsp?">&laquo; Zur&uuml;ck zur Adminseite</a>
					</p>
					</div>
				</form>

				<div id="crudTable">
					<h2>Alle Auftr&auml;ge:</h2>
					<table>
						<tr>
							<th>Auftragsnr.</th>
							<th>UserID</th>
							<th>Rechnungsdatum</th>
							<th>Summe</th>
							<th colspan=2>Aktionen</th>
							<th>Auftragspos.</th>
						</tr>

						<%-- Alle Auftr&auml;ge ausgeben --%>
						<c:forEach items="${auftragliste}" var="auftragBean">
							<tr>
								<td>${auftragBean.auftragsnummer}</td>
								<td>${auftragBean.userID}</td>
								<td>${auftragBean.datum}</td>
								<td class="summe">${auftragBean.rechnungssumme}</td>

								<%-- Auftrag L&ouml;schen --%>
								<td><a
									href="../../DeleteAuftragServlet?auftragsnummer=${auftragBean.auftragsnummer}"><img alt="AuftragL&ouml;schen" src="../../img/loeschen.png"></a></td>

								<%-- Auftrag Updaten --%>
								<td><a href="updateAuftrag.jsp?alteAuftragsnummer=${auftragBean.auftragsnummer}&alteUserID=${auftragBean.userID}&altesDatum=${auftragBean.datum}&alteRechnungssumme=${auftragBean.rechnungssumme}">
									<img alt="AuftragBearbeiten"
										src="../../img/bearbeiten.png"></a></td>
								
								<%-- Auftragspositionen anzeigen --%>
								<td><a
									href="../../ReadAllAuftragspositionServlet?auftragsnummer=${auftragBean.auftragsnummer}">
									<img alt="auftragspositionenAnzeigen" src="../../img/anzeigen.png"></a></td>
							</tr>
						</c:forEach>
					</table>
						<br>
				</div>
			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>