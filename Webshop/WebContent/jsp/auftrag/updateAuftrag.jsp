<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<title>Auftr&auml;ge bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/updateStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="updateAuftragForm" method="post"
					action="../../UpdateAuftragServlet" accept-charset="utf-8">

					<div class="bearbeitung">
						<h2>Auftrag bearbeiten:</h2>
						<input type="hidden" name="alteAuftragsnummer" id="alteAuftragsnummer"
							value="${param.alteAuftragsnummer}">

						<table>
							<tr>
								<td><label for="Auftragsnummer">Neue Auftragsnummer:</label></td>
								<td><input type="text" name="neueAuftragsnummer" id="neueAuftragsnummer"
									value="${param.alteAuftragsnummer}" required></td>
							</tr>
							<tr>
								<td><label for="userID">Neue UserID:</label></td>
								<td><input type="text" name="userID" id="userID"
									value="${param.alteUserID}" required></td>
							</tr>
							<tr>
								<td><label for="datum">Neues Rechnungsdatum:</label></td>
								<td><input type="date" name="datum" id="datum"
									value="${param.altesDatum}" required></td>	
							</tr>
							<tr>
								<td><label for="rechnungssumme">Neue Rechnungssumme:</label></td>
								<td><input type="text" name="rechnungssumme" id="rechnungssumme"
									value="${param.alteRechnungssumme}" pattern="\d+(\.\d{2})?" required></td>
							</tr>
						</table>

						<br>
						<button name="submit" type="submit" id="submit">Absenden</button>
						<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
					</div>

					<div>
							<a class="zurueck" href="../../ReadAllAuftragServlet">Zur&uuml;ck zur
								Auftrags-&Uuml;bersicht</a>
					</div>
				</form>

			</main>

		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>