<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${not empty produktliste}">

		<div class="produkte">

			<c:forEach items="${produktliste}" var="produktBean">

				<div class="produkt ${produktBean.kategorie}">

					<div class="produktInfo">

						<form method="get" action="../../ReadProduktServlet">
							<p class="artNr">ArtikelNr: ${produktBean.id}</p>
							<input type="image" class="img"
								src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
								alt="${produktBean.bezeichnung}">
							<hr>
							<p>${produktBean.bezeichnung}</p>
							<c:choose>
								<c:when test="${not produktBean.sale}">
									<span class="preis">${produktBean.preis}&euro;</span>
								</c:when>
								<c:when test="${produktBean.sale}">
									<span class="alterPreis">${produktBean.originalpreis}&euro;</span>
									<span class="salepreis">${produktBean.preis}&euro; </span>
								</c:when>
							</c:choose>
							<input type="hidden" id="produktID" name="produktID"
								value="${produktBean.id}">
						</form>

					</div>

				</div>

			</c:forEach>

		</div>

	</c:when>
	<c:otherwise>
		<br>
		<br>
		<span class="empty">Leider ergab ihre Suche keine Treffer!</span>
	</c:otherwise>


</c:choose>