<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Shopseite</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/produkte/shopseite.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<script src="../../js/ajax/login.js"></script>
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/cookie.js"></script>
<script src="../../js/allgemein/navigation.js"></script>
<script src="../../js/ajax/filter.js"></script>
</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>
	<nav>
		<%@ include file="../../jspf/kategorieFragment.jspf"%>
	</nav>
	<main>
	<div id="flex">
		<div id="filterProdukte">
			<form autocomplete="off">
				<div class="filterbereich">
					<p>Produkte filtern:</p>
					<div class="preisBox">
						<label for="preisRadio">Preis-Sortierung:</label> <select
							name="preis" id="preis" class="preisSortierung">
							<option value=""></option>
							<option class="filterSortierung" value="ASC" id="ASC">Auf</option>
							<option class="filterSortierung" value="DESC" id="DESC">Ab</option>
						</select><br>
					</div>
					<div class="preisSpanne">
						<label for="minPreis">Min. Preis:</label> <input type="number"
							id="minPreis" name="minPreis" placeholder="Preis eingeben...">
						<button type="button" class="suchButton">&raquo;</button>
						<br> <label for="maxPreis">Max. Preis:</label> <input
							type="number" id="maxPreis" name="maxPreis"
							placeholder="Preis eingeben...">
						<button type="button" class="suchButton">&raquo;</button>
						<br>
					</div>
					<label for="farbe">Farbe:</label><br> <select name="farbe"
						id="farbe">
						<option value="">-Alle Farben-</option>
						<c:forEach items="${farbeliste}" var="farbeBean">
							<option value="${farbeBean.bezeichnung}" class="filterFarbe"
								id="${farbeBean.bezeichnung}">${farbeBean.bezeichnung}</option>
						</c:forEach>
					</select> <input type="hidden" id="kategorie" name="kategorie"
						value="${param.kategorie}">
				</div>
			</form>
		</div>
		<div id="filterTable">
			<c:choose>
				<c:when test="${not empty produktliste}">

					<div class="produkte">

						<c:forEach items="${produktliste}" var="produktBean">

							<div class="produkt ${produktBean.kategorie}">

								<div class="produktInfo">

									<form method="get" action="../../ReadProduktServlet">
										<p class="artNr">ArtikelNr: ${produktBean.id}</p>
										<input type="image" class="img"
											src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
											alt="${produktBean.bezeichnung}">
										<hr>
										<p>${produktBean.bezeichnung}</p>
										<c:choose>
											<c:when test="${not produktBean.sale}">
												<span class="preis">${produktBean.preis}&euro;</span>
											</c:when>
											<c:when test="${produktBean.sale}">
												<span class="alterPreis">${produktBean.originalpreis}&euro;</span>
												<span class="salepreis">${produktBean.preis}&euro; </span>
											</c:when>
										</c:choose>
										<input type="hidden" name="produktID"
											value="${produktBean.id}">
									</form>

								</div>

							</div>

						</c:forEach>

					</div>
				</c:when>
				<c:otherwise>
					<br>
					<br>
					<span class="empty">Leider ergab ihre Suche keine Treffer!</span>
				</c:otherwise>


			</c:choose>
		</div>
		</div>
	</main>

	<footer>
		<%@ include file="../../jspf/footerFragment.jspf"%>

	</footer>
</body>
</html>