<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Produkt bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/updateStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>

</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="updateProduktForm" method="post"
					action="../../UpdateProduktServlet" enctype=multipart/form-data
					accept-charset="utf-8">

					<div class="bearbeitung">
						<h2>Produkt bearbeiten:</h2>
						<input type="hidden" name="alteID" id="alteID"
							value="${produktBean.id}">

						<table id="updateProduktTable">
							<tr>
								<td><label for="ID">Neue ID:</label></td>
								<td><input type="text" name="neueID" id="neueID"
									value="${produktBean.id}"></td>
							</tr>
							<tr>
								<td><label for="bezeichnung">Neue Bezeichnung:</label></td>
								<td><input type="text" name="neueBezeichnung"
									id="neueBezeichnung" value="${produktBean.bezeichnung}"></td>
							</tr>
							<tr>
								<td><label for="preis">Neuer Preis:</label></td>
								<td><input type="text" name="neuerPreis" id="neuerPreis"
									value="${produktBean.preis}" pattern="\d+(\.\d{2})?"></td>
							</tr>
							<tr>
								<td><label for="hoehe">Neue H&ouml;he:</label></td>
								<td><input type="number" name="neueHoehe" id="neueHoehe"
									value="${produktBean.hoehe}"></td>
							</tr>
							<tr>
								<td><label for="breite">Neue Breite:</label></td>
								<td><input type="number" name="neueBreite" id="neueBreite"
									value="${produktBean.breite}"></td>
							</tr>
							<tr>
								<td><label for="tiefe">Neue Tiefe:</label></td>
								<td><input type="number" name="neueTiefe" id="neueTiefe"
									value="${produktBean.tiefe}"></td>
							</tr>
							<tr>
								<td><label for="gewicht">Neues Gewicht:</label></td>
								<td><input type="text" name="neuesGewicht"
									id="neuesGewicht" value="${produktBean.gewicht}"></td>
							</tr>
							<tr>
								<td><label for="beschreibung">Neue Beschreibung:</label></td>
								<td><input type="text" name="neueBeschreibung"
									id="neueBeschreibung" value="${produktBean.beschreibung}">
								</td>
							</tr>
							<tr>
								<td><label for="lagerbestand">Neuer Lagerbestand:</label></td>
								<td><input type="number" name="neuerLagerbestand"
									id="neuerLagerbestand" value="${produktBean.lagerbestand}">
								</td>
							</tr>
							<tr>
								<td><label for="sale"> Produkt im Sale? </label><br></td>
								<td><input type="radio" class="radio" id="sale" name="sale"
									value="true"
									${produktBean.sale == 'true' ? 'checked="checked"':''}>
									<label for="sale">Ja</label> <input type="radio" class="radio"
									id="sale" name="sale" value="false"
									${produktBean.sale == 'false' ? 'checked="checked"':''}>
									<label for="sale">Nein</label><br></td>
							</tr>
							<tr>
								<td><label for="originalpreis">Urspr&uuml;nglicher
										Preis:</label></td>
								<td><input type="text" name="neuerOriginalpreis"
									id="neuerOriginalpreis" value="${produktBean.originalpreis}"
									pattern="\d+(\.\d{2})?"></td>
							</tr>
							<tr>
								<td><label for="kategorie">Kategorie:</label></td>
								<td><select name="kategorie" id="kategorie">
										<option selected>${produktBean.kategorie}</option>
										<c:forEach items="${kategorieliste}" var="kategorieBean">
											<option value="${kategorieBean.bezeichnung}">${kategorieBean.bezeichnung}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td><label for="farbe">Farbe:</label></td>
								<td><select name="farbe" id="farbe">
										<option selected>${produktBean.farbe}</option>
										<c:forEach items="${farbeliste}" var="farbeBean">
											<option value="${farbeBean.bezeichnung}">${farbeBean.bezeichnung}</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td><label for="filename">Neuer Filename:</label></td>
								<td><input type="text" name="filename" id="filename"
									value="${produktBean.filename}"></td>
							</tr>
							<tr>
								<td><label>Aktuelles Produktbild:</label></td>

								<td><div id="imgContainer">
										<img class="img"
											src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
											alt="${produktBean.bezeichnung}">
									</div>
							</tr>
							<tr>
								<td><label for="neuesBild">Neues Produktbild:</label></td>
								<td><label for="neuesBild">Bild hochladen:</label> <input
									type="file" name="neuesBild" id="neuesBild" accept="image/*"></td>
							</tr>
						</table>
						<br>
						<button name="submit" type="submit" id="submit">Absenden</button>
						<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
					</div>
					<a class="zurueck" href="../../ReadAllProduktServlet">&laquo;
						Zur&uuml;ck zur Produkt-&Uuml;bersicht</a>
				</form>
			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>