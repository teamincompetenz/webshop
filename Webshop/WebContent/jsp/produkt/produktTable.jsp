<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- Produktetabelle -->
<div id="crudTable" class="crudProdukt">
	<h2>Alle Produkte:</h2>
	<table>
		<tr>
			<th>Bild</th>
			<th>Id</th>
			<th>Bezeichnung</th>
			<th>Preis</th>
			<th>H&ouml;he</th>
			<th>Breite</th>
			<th>Tiefe</th>
			<th>Gewicht</th>
			<th>Beschreibung</th>
			<th>Sale</th>
			<th>Urspr.</th>
			<th>Lager</th>
			<th colspan=2>Aktionen</th>
		</tr>

		<%-- Alle Produkte ausgeben --%>
		<c:forEach items="${produktliste}" var="produktBean">
			<tr>
				<td class="produktBild">
					<div id="imgContainer">
						<img class="img"
							src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
							alt="${produktBean.bezeichnung}">
					</div>
				</td>
				<td>${produktBean.id}</td>
				<td class="beschr">${produktBean.bezeichnung}</td>
				<td>${produktBean.preis}&euro;</td>
				<td>${produktBean.hoehe}cm</td>
				<td>${produktBean.breite}cm</td>
				<td>${produktBean.tiefe}cm</td>
				<td>${produktBean.gewicht}kg</td>
				<td class="beschr">${produktBean.beschreibung}</td>
				<td>${produktBean.sale}</td>
				<td>${produktBean.originalpreis}&euro;</td>
				<td class="lagerbestand">${produktBean.lagerbestand}Stk.</td>


				<%-- Produkte L&ouml;schen --%>
				<td><a
					href="../../DeleteProduktServlet?id=${produktBean.id}&reload=true">
						<img alt="ProduktL&ouml;schen" src="../../img/loeschen.png">
				</a></td>

				<%-- Produkte Updaten --%>
				<td><a
					href="../../UpdateProduktVorbereitenServlet?id=${produktBean.id}"><img
						alt="ProduktBearbeiten" src="../../img/bearbeiten.png"></a></td>
			</tr>
		</c:forEach>
	</table>
</div>
<br>

