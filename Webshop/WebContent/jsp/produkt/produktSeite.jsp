<!-- Verantwortlich: Daniel Fina | Felix Gröhlich (Zeilen 115 bis 122) -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>${produktBean.bezeichnung}</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/produkte/produktseite.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<script src="../../js/ajax/login.js"></script>
<script src="../../js/allgemein/cookie.js"></script>
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/produktSeite.js"></script>
</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>

	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%>
	</nav>

	<main>
		<div id="produkt">
			<div id="imgContainer">
				<img class="img"
					src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
					alt="${produktBean.bezeichnung}">
			</div>
			<!-- Produktdetails auf der rechten Seite anzeigen -->
			<div id="produktDetails">
				<h1>${produktBean.bezeichnung}</h1>
				<hr>
				<br>
				<table>
					<c:choose>
						<c:when test="${produktBean.sale}">
							<tr>
								<td class="urPreis">Urspr&uuml;nglicher VP:</td>
								<td class="urPreisText">${produktBean.originalpreis}&euro;</td>
							</tr>
							<tr>
								<td class="neuPreis">Neuer Preis:</td>
								<td class="neuPreisText">${produktBean.preis}&euro;</td>
							</tr>
							<tr>
								<td class="sparPreis">Sie sparen:</td>
								<td class="sparPreisText">${produktBean.originalpreis - produktBean.preis}&euro;</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td class="preis">${produktBean.preis}&euro;</td>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<td class="preisMwst">inkl. Steuer || zzgl. Versandkosten</td>
					</tr>
				</table>
				<br>
				<form id="warenkorb" method="get" action="../../AddToCardServlet">
					<p>Lagerbestand: <span>${produktBean.lagerbestand} St&uuml;ck</span></p>
					<p class="reserviert">Reserviert: ${reserviert}</p>
					
					<!-- Entscheiden, ob User eingeloggt ist, ob Produkt verfügbar / reserviert ist -->
					<div id="entscheidung">
						<c:choose>
							<c:when test="${user != null}">
								<c:if
									test="${(produktBean.lagerbestand != 0) and (reserviertbool != true)}">
									<label class="anzahl" for="anzahl">Anzahl:</label>
									<input class="anzahlNummer" type="number" id="anzahl"
										name="anzahl" min="1"
										max="${produktBean.lagerbestand - reserviert}" value="1"><br>
									<button type="button" class="wkButton" id="wkButton">&raquo;
										In den Warenkorb</button>
								</c:if>
								<c:if test="${produktBean.lagerbestand == 0}">
									<span class="ausverkauft">&raquo; Dieses Produkt ist
										leider ausverkauft! </span>
								</c:if>
								<c:if test="${reserviertbool}">
									<span class="reserviertText">&raquo; Dieses Produkt ist
										leider reserviert! </span>
								</c:if>
							</c:when>
							<c:otherwise>
								<span class="loggedIn">&raquo; Sie m&uuml;ssen angemeldet
									sein,<br> um etwas in den Warenkorb zu legen
								</span>
							</c:otherwise>
						</c:choose>
					</div>
					<br>
					<p>Beschreibung:</p><span>${produktBean.beschreibung}</span>
					<br><br>
					<p>Farbe: <span>${produktBean.farbe}</span></p>
					<br> 
					<p>Ma&szlig;e: <span>${produktBean.breite} x ${produktBean.hoehe} x ${produktBean.tiefe} cm</span></p>  
					<br>
					<p>Gewicht: <span>${produktBean.gewicht} kg</span></p> 
					<br> 
					
					<!-- Verantwortlich: Felix Gröhlich (Zeilen: 115 bis 122) -->
					<input type="hidden" id="produktID"
						name="produktID" value="${produktBean.id}"> <input
						type="hidden" id="produktPreis" name="produktPreis"
						value="${produktBean.preis}"> <input type="hidden"
						id="lagerbestand" name="lagerbestand"
						value="${produktBean.lagerbestand}"> <input type="hidden"
						id="bezeichnung" name="bezeichnung"
						value="${produktBean.bezeichnung}">
				</form>
			</div>
		</div>
	</main>

	<footer>
		<%@ include file="../../jspf/footerFragment.jspf"%>
	</footer>
</body>
</html>