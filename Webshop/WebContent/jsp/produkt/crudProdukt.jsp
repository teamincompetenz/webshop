<!-- Verantwortlich: Jan Hermann -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Produkte Bearbeiten</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/allgemein/crudSubmit.js"></script>
<script src="../../js/ajax/produktSearch.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<form id="createProduktForm" method="post"
					action="../../CreateProduktServlet" enctype=multipart/form-data
					accept-charset="utf-8">

					<%-- Produkt erstellen --%>
					<div class="bearbeitung">
						<h2>Neues Produkt anlegen:</h2>
						<label for="bezeichnung">Bezeichnung:</label><br> <input
							type="text" name="bezeichnung" id="bezeichnung"
							placeholder="Produktbezeichnung..." required><br> <label
							for="preis">Preis:</label><br> <input type="text"
							name="preis" id="preis" placeholder="Betrag eingeben 0.00"
							pattern="\d+(\.\d{2})?" required><br> <label
							for="hoehe">H&ouml;he:</label><br> <input type="number"
							name="hoehe" id="hoehe" placeholder="H&ouml;he in cm eingeben"
							step="1" required><br> <label for="breite">Breite:</label><br>
						<input type="number" name="breite" id="breite"
							placeholder="Breite in cm eingeben" step="1" required><br>

						<label for="tiefe">Tiefe:</label><br> <input type="number"
							name="tiefe" id="tiefe" placeholder="Tiefe in cm eingeben"
							step="1" required><br> <label for="gewicht">Gewicht:</label><br>
						<input type="text" name="gewicht" id="gewicht"
							placeholder="Gewicht in Kg eingeben 0.00" pattern="\d+(\.\d{2})?"
							required><br> <label for="beschreibung">Beschreibung:</label><br>
						<input type="text" name="beschreibung" id="beschreibung"
							placeholder="Produkt beschreiben" required><br> <label
							for="lagerbestand">Lagerbestand:</label><br> <input
							type="number" name="lagerbestand" id="lagerbestand" step="1"
							placeholder="Lagerbestand eingeben" step="1" required> <br>

						<label for="sale"> Produkt im Sale? </label><br> <input
							type="radio" class="radio" id="sale" name="sale" value="True">
						<label for="sale">Ja</label> <input type="radio" class="radio"
							id="sale" name="sale" value="False" checked="checked"> <label
							for="sale">Nein</label><br> <label for="originalpreis">Urspr&uuml;nglicher
							Preis:</label><br> <input type="text" name="originalpreis"
							id="originalpreis" placeholder="Betrag eingeben 0.00"
							pattern="\d+(\.\d{2})?" required><br> <label
							for="kategorie">Kategorie:</label><br> <select
							name="kategorie" id="kategorie" required>
							<option selected></option>
							<c:forEach items="${kategorieliste}" var="kategorieBean">
								<option value="${kategorieBean.bezeichnung}">${kategorieBean.bezeichnung}</option>
							</c:forEach>
						</select><br> <label for="farbe">Farbe:</label><br> <select
							name="farbe" id="farbe" required>
							<option selected></option>
							<c:forEach items="${farbeliste}" var="farbeBean">
								<option value="${farbeBean.bezeichnung}">${farbeBean.bezeichnung}</option>
							</c:forEach>
						</select><br> <label for="image">Bild hochladen:</label> <input
							type="file" name="image" id="image" accept="image/*" required
							multiple>
						<div class="buttonContainer">
							<button name="submit" type="submit" id="submit">Absenden</button>
							<button name="reset" type="reset" id="reset">Zur&uuml;cksetzen</button>
						</div>
						<p>
							<a class="zurueck" href="../allgemein/admin.jsp?">&laquo;
								Zur&uuml;ck zur Adminseite</a>
						</p>
					</div>
				</form>

				<div id="crudTable" class="crudProdukt">
					<h2>Alle Produkte:</h2>
					<table>
						<tr>
							<th>Bild</th>
							<th>Id</th>
							<th>Bezeichnung</th>
							<th>Preis</th>
							<th>H&ouml;he</th>
							<th>Breite</th>
							<th>Tiefe</th>
							<th>Gewicht</th>
							<th>Beschreibung</th>
							<th>Sale</th>
							<th>Urspr.</th>
							<th>Lager</th>
							<th colspan=2>Aktionen</th>
						</tr>

						<%-- Alle Produkte ausgeben --%>
						<c:forEach items="${produktliste}" var="produktBean">
							<tr>
								<td class="produktBild">
									<div id="imgContainer">
										<img class="img"
											src="../../BildAnzeigenServlet?bildId=${produktBean.id}"
											alt="${produktBean.bezeichnung}">
									</div>
								</td>
								<td>${produktBean.id}</td>
								<td class="beschr">${produktBean.bezeichnung}</td>
								<td>${produktBean.preis}&euro;</td>
								<td>${produktBean.hoehe}cm</td>
								<td>${produktBean.breite}cm</td>
								<td>${produktBean.tiefe}cm</td>
								<td>${produktBean.gewicht}kg</td>
								<td class="beschr">${produktBean.beschreibung}</td>
								<td>${produktBean.sale}</td>
								<td>${produktBean.originalpreis}&euro;</td>
								<td class="lagerbestand">${produktBean.lagerbestand}Stk.</td>


								<%-- Produkte L&ouml;schen --%>
								<td><a
									href="../../DeleteProduktServlet?id=${produktBean.id}">
										<img alt="ProduktL&ouml;schen" src="../../img/loeschen.png">
								</a></td>

								<%-- Produkte Updaten --%>
								<td><a
									href="../../UpdateProduktVorbereitenServlet?id=${produktBean.id}"><img
										alt="ProduktBearbeiten" src="../../img/bearbeiten.png"></a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<form>
					<div id="produktsuche" class="bearbeitung">
						<label for="suche">Suche nach Produktbezeichnung:</label><br>
						<input type="text" name="suche" id="suche"
							placeholder="Produktbezeichnung..."><br>
					</div>
				</form>
			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>