<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Verm&ouml;bler Adminseite</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/admin.css" rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>

			<main>
				<div id="adminButtonContainer">
					<a href="../../ReadAllUserServlet" id="userLink">User bearbeiten</a> 
					<a href="../../ReadAllProduktServlet" id="produktLink">Produkt bearbeiten</a> 
					<a href="../../ReadAllKategorieServlet" id="kategorieLink">Kategorie bearbeiten</a> 
					<a href="../../ReadAllFarbeServlet" id="farbeLink">Farbe bearbeiten</a> 
					<a href="../../ReadAllAuftragServlet" id="auftragLink">Auftr&auml;ge bearbeiten</a> 
					<a href="../../ReadAllKontaktServlet" id="kontaktLink">Kontakte bearbeiten</a>
				</div>
			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zurück zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>