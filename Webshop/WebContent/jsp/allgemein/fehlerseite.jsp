<!-- Verantwortlich: Jan Hermann -->
<%@ page isErrorPage="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="${pageContext.request.requestURI}" />
<meta content="text/html; charset=UTF-8">
<title>Fehlerseite</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/fehlerseite.css" rel="stylesheet">
</head>
<body>
	<main>
		<div id="imgContainer">
			<img src="../../img/error.png" alt="errorBild">
		</div>
		<div id="errorContainer">
			<h1>Fehlerseite</h1>
			<h2>
				Ooopsie Dooopsie,<br> was ist denn da nur passiert?
			</h2>
			<c:choose>
				<c:when test="${exeption}">
					<p>Exeption: ${exeptionNachricht}</p>
				</c:when>
				<c:otherwise>
					<p>Die Fehlermeldung lautet: ${pageContext.exception}</p>
				</c:otherwise>
			</c:choose>
			<p>
				<a href="../../index.html">&raquo; Hier &laquo;</a> geht es
				zur&uuml;ck zur Startseite
			</p>
		</div>
	</main>
</body>
</html>