<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8" http-equiv="refresh" content="20;url=../../">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Verm&ouml;bler Kontakt</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/allgemein/navigation.css" rel="stylesheet">
<link href="../../css/allgemein/footer.css" rel="stylesheet">
<link href="../../css/allgemein/popup.css" rel="stylesheet">
<link href="../../css/footer/kontakt.css" rel="stylesheet">
<script src="../../js/ajax/login.js"></script>
<script src="../../js/allgemein/header.js"></script>
<script src="../../js/allgemein/cookie.js"></script>
</head>
<body>
	<header>
		<%@ include file="../../jspf/headerFragment.jspf"%>
	</header>

	<nav>
		<%@ include file="../../jspf/navFragment.jspf"%>
	</nav>

	<main>
		<div id="kontaktausgabe">
			<h2>
				<span>${kontaktBean.vorname}</span>, vielen Dank für deine Anfrage!
				(Anfrage-ID: ${kontaktBean.id})
			</h2>

			<p class="nachricht">
				Wir haben
				<c:choose>
					<c:when test="${kontaktBean.grund == 'verbesserung'}">
						<span> deinen Verbesserungsvorschlag</span>
					</c:when>
					<c:when test="${kontaktBean.grund == 'beschwerde'}">
						<span> deine Beschwerde</span>
					</c:when>
					<c:otherwise>
						<span> deine Nachricht</span>
					</c:otherwise>
				</c:choose>
				erhalten:
			</p>
			<p class="textNachricht">"${kontaktBean.nachricht}"</p>

			<br> <br>
			<p>F&uuml;r m&ouml;gliche Fragen wenden wir uns an deine
				hinterlegte E-Mail Adresse.</p>
			<br> <br>
			<p>Du wirst in 20 Sekunden wieder zur Startseite geleitet!</p>
			<p>Drück <a href="../../">hier</a>, um zur Startseite zu gelangen</p>

		</div>
		<hr>
	</main>

	<footer>
		<%@ include file="../../jspf/footerFragment.jspf"%>
	</footer>
</body>
</html>