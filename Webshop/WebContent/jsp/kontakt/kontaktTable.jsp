<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- Usertabelle -->
<div id="crudTable">
	<h2>Alle Kontakteing&auml;nge:</h2>
	<table>
		<tr>
			<th>Id</th>
			<th>Titel</th>
			<th>Vorname</th>
			<th>Nachname</th>
			<th>Mail</th>
			<th>Grund</th>
			<th>Nachricht</th>
			<th>L&ouml;schen</th>
		</tr>

		<c:forEach var="kb" items="${kontaktliste}">
			<tr>
				<td>${kb.id}</td>
				<td>${kb.titel}</td>
				<td>${kb.vorname}</td>
				<td>${kb.nachname}</td>
				<td>${kb.mail}</td>
				<td>${kb.grund}</td>
				<td class="kontaktNachricht">${kb.nachricht}</td>

				<%-- User L&ouml;schen --%>
				<td><a href="../../DeleteKontaktServlet?id=${kb.id}&delete=true"><img
						alt="KontaktL&ouml;schen" src="../../img/loeschen.png"></a></td>

			</tr>
		</c:forEach>
	</table>
</div>
<br>

