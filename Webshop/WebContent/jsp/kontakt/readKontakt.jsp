<!-- Verantwortlich: Daniel Fina -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="../allgemein/fehlerseite.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang=de>
<head>
<base href="${pageContext.request.requestURI }" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Kontaktliste</title>
<link href="../../css/allgemein/design.css" rel="stylesheet">
<link href="../../css/allgemein/header.css" rel="stylesheet">
<link href="../../css/crud/crudStyle.css" rel="stylesheet">
<script src="../../js/ajax/kontaktSearch.js"></script>
<script src="../../js/allgemein/crudSubmit.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${userBean.admin}">
			<header>
				<%@ include file="../../jspf/adminHeaderFragment.jspf"%>
			</header>
			<main>
				<div class="bearbeitung">
					<h2>Kontakt suchen:</h2>
					<form autocomplete="off">
						<div class="eingabeContainer">
							<label for="grund">Grund des Kontakts:</label><br> 
							<select name="grund" id="grund">
							<option value="">Hier ausw&auml;hlen</option>
							<option value="verbesserung">Verbesserung</option>
							<option value="beschwerde">Beschwerde</option>
							<option value="sonstiges">Sonstiges</option>
							</select><br> <label
								for="mail">E-Mail:</label><br> <input type="text"
								name="mail" id="mail" placeholder="E-Mail eingeben...">
							<p>
								<a class="zurueck" href="../allgemein/admin.jsp">&laquo;
									Zur&uuml;ck zur Adminseite</a>
							</p>
						</div>
					</form>
				</div>
				<!-- Usertabelle -->
				<div id="crudTable" class="crudKontakt">
					<h2>Alle Kontakteing&auml;nge:</h2>
					<table>
						<tr>
							<th>Id</th>
							<th>Titel</th>
							<th>Vorname</th>
							<th>Nachname</th>
							<th>Mail</th>
							<th>Grund</th>
							<th>Nachricht</th>
							<th>L&ouml;schen</th>
						</tr>

						<c:forEach var="kb" items="${kontaktliste}">
							<tr>
								<td>${kb.id}</td>
								<td>${kb.titel}</td>
								<td>${kb.vorname}</td>
								<td>${kb.nachname}</td>
								<td>${kb.mail}</td>
								<td>${kb.grund}</td>
								<td class="kontaktNachricht">${kb.nachricht}</td>

								<%-- User L&ouml;schen --%>
								<td><a href="../../DeleteKontaktServlet?id=${kb.id}&delete=true"><img
										alt="KontaktL&ouml;schen" src="../../img/loeschen.png"></a></td>

							</tr>
						</c:forEach>
					</table>
				</div>
				<br>

			</main>
		</c:when>
		<c:when test="${not userBean.admin}">
			<p>Sie haben leider keinen Zugriff auf diese Seite</p>
			<p>
				Hier geht es zur&uuml;ck zur <a href="../../index.html">Startseite</a>
			</p>
		</c:when>
	</c:choose>
</body>
</html>