/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", checkCookie);

// Abfrage, ob Cookies deaktiviert sind und ggfs. Span-Feld erstellen
function checkCookie() {
	if (!navigator.cookieEnabled) {
		var textfeld = document.createElement("span");
		textfeld.innerHTML = "Sie haben ihre Cookies in den Browsereinstellungen deaktiviert. Bitte aktivieren Sie diese, um die Seite nutzen zu können";
		document.body.appendChild(textfeld);
		textfeld.classList.add("cookieTextFeld");
	}
}