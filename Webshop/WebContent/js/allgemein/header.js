/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	var konto = document.getElementById("kontoButton");
	if (konto !== null) {
		konto.addEventListener("click", PopUp);
	}
	var close = document.getElementById("popupClose");
	if (close !== null) {
		close.addEventListener("click", formClose);
	}
	document.body.addEventListener("click", formHandler);
}

// Popup öffnen lassen mit class = active setzen
function PopUp() {
	document.getElementById("popup").classList.toggle("active");
}

// Popup schließen, indem class nicht mehr active
function formClose() {
	var form = document.getElementById("popup");
	if (form != null) {
		form.classList.remove("active");
	}
}

// formClose, wenn nicht auf Kontobutton, X oder in Popup geklickt wird
function formHandler(event) {
	if (event.target.id !== 'kontoButton' && event.target.id !== "loginForm"
			&& event.target.parentElement.id !== "loginForm") {
		formClose();
	}

}
