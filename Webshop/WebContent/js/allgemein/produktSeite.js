/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	document.getElementById("wkButton").addEventListener("click", setAlert);
}

// PreventDefault falls nicht sicher, sonst Nachricht zeigen und submit
function setAlert() {
	var sicher = confirm("Produkt in den Warenkorb hinzufügen?");
	if (!sicher) {
		preventDefault();
	} else {
		alert("Erfolgreich hinzugefügt! Sie werden auf die Shopseite weitergeleitet");
		document.getElementById("warenkorb").submit();
	}

}