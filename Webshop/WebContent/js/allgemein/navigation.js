/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	var buttons = document.querySelectorAll('button.nav');
	var url = new URL(window.location.href);
	var kategorie = url.searchParams.get("kategorie");

	// Active-Class anhand der Kategorie setzen
	for (let i = 0; i < buttons.length; i++) {
		if (buttons[i].getAttribute("name") === "kategorie"
				&& buttons[i].getAttribute("value") === kategorie) {
			buttons[i].classList.add("active");
		} else {
			buttons[i].classList.remove("active");
		}
	}
	if (kategorie === null || kategorie === "") {
		buttons[0].classList.add("active");
	}
}
