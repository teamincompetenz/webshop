/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	document.getElementById("resetButton").addEventListener("click", checkReset);
}

function checkReset() {
	var sicher = confirm("Daten sicher zurücksetzen?");
	if (!sicher) {
		preventDefault();
	}
}
