/*
 * Verantwortlicher: Jan Hermann
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	var images = document.getElementsByClassName("slideshowBilder");

	// Alle Bilder ausblenden
	for (var i = 0; i < images.length; i++) {
		images[i].style.display = "none";
	}

	var index = 0;

	// Beim Erstaufruf wird das erste Bild angezeigt, danach nicht mehr
	if (!init.erstaufruf) {
		init.erstaufruf = true;
	} else {
		// Zufälliges Bild auswählen
		index = Math.floor(Math.random() * 11) + 1;
	}

	// Bild anzeigen
	images[index].style.display = "block";

	// Alle vier Sekunden ein neues Bild
	setTimeout(init, 4000);
}