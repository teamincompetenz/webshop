/*
 * Verantwortlicher: Daniel Fina
 */

document.addEventListener("DOMContentLoaded", init);

function init() {
	var submit = document.getElementById("submit");
	// Check for Null wegen false positiv Fehlermeldung von Firefox
	if (submit != null) {
		submit.addEventListener("click", checkSubmit);
	}
	var reset = document.getElementById("reset");
	if (reset != null) {
		reset.addEventListener("click", checkReset);
	}
}

function checkSubmit(evt) {
	var sicher = confirm("Eingegebenen Daten korrekt?");
	if (!sicher) {
		evt.preventDefault();
	}
}

function checkReset() {
	var sicher = confirm("Daten sicher zurücksetzen?");
	if (!sicher) {
		evt.preventDefault();
	}
}