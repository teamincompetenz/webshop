/*
 * Verantwortlicher: Jan Hermann
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);
function init() {
	document.getElementById("suche").addEventListener("keyup", changeContent);
}

function changeContent() {
	var searchURL = "../../ReadAllProduktServlet";
	var suche = document.getElementById("suche").value;
	if (suche != null && suche.length > 0) { 
		searchURL += "?suche=" + encodeURIComponent(suche);
	}

	var xmlhttp = new XMLHttpRequest();
	xmlhttp.addEventListener("load", function() {
		document.getElementById("crudTable").innerHTML = xmlhttp.responseText;
	});
	xmlhttp.open("GET", searchURL, true);
	xmlhttp.send();
}