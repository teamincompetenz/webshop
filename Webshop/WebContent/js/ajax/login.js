/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	document.getElementById("loginButton").addEventListener("click", checkLogin);
}

//checkLogin via AJAX
function checkLogin() {
	var mail = document.getElementById("mail").value;
	var pass = document.getElementById("passwort").value;
	var searchURL = "../../LoginServlet";
	
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function() {
		console.log("Callback reached with status " + xmlhttp.status
				+ " and readyState " + xmlhttp.readyState);
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				var result = xmlhttp.responseText;
				if (result == 'true') {
					alert("Sie wurden erfolgreich eingeloggt!");
					document.getElementById("loginForm").submit();
				} else {
					document.getElementById("textLogin").innerHTML = "E-Mail oder Passwort falsch";			
			}
	}
};
xmlhttp.open("POST", searchURL, true);
xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
xmlhttp.send("formSubmit=no&mail=" + mail + "&passwort=" + pass);
}