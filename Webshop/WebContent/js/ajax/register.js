/*
 * Verantwortlicher: Daniel Fina
 */

document.addEventListener("DOMContentLoaded", init);

function init() {
	document.getElementById("mail").addEventListener("blur", checkMail);
	document.getElementById("registerForm")
			.addEventListener("keyup", checkCaps);
	document.getElementById("registerForm")
			.addEventListener("keyup", checkCaps);
	document.getElementById("registerForm").addEventListener("reset",
			checkReset);
	document.getElementById("registerButton").addEventListener("click",
			checkForm);
}
// Reset Button überprüfung
function checkReset(evt) {
	var sicher = confirm("Daten sicher zurücksetzen?");
	if (!sicher) {
		evt.preventDefault();
	} else {
		document.getElementById("mailText").innerHTML = "";
		document.getElementById("gebText").innerHTML = "";
		document.getElementById("passwortText").innerHTML = "";
	}
}

// EMail überprüfung mittels AJAX
var checkMailBoolean = false;
function checkMail() {
	var mail = document.getElementById("mail").value;
	var searchURL = "../../RegisterServlet";
	if (mail != null && mail.length > 0)
		searchURL += "?mail=" + encodeURIComponent(mail);
	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		console.log("Callback reached with status " + xmlhttp.status
				+ " and readyState " + xmlhttp.readyState);
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var result = xmlhttp.responseText;
			if (mail === "") {
				document.getElementById("mailText").innerHTML = "&cross; Bitte E-Mail eingeben!";
				checkMailBoolean = false;
			} else if (result == 'true') {
				document.getElementById("mailText").innerHTML = "";
				checkMailBoolean = true;
			} else {
				document.getElementById("mailText").innerHTML = "&cross; E-Mail schon vergeben!";
				document.getElementById("mailText").removeAttribute("class");
				checkMailBoolean = false;
			}
		}
	};
	xmlhttp.open("GET", searchURL, true);
	xmlhttp.send();
}
// Geburtsdatum überprüfen
function checkGeb() {
	var geb = new Date(document.getElementById("geburtstag").value);
	var heute = new Date();
	var mindestDatum = new Date(heute.getFullYear() - 120, 01, 01);
	var maxDatum = new Date(heute.getFullYear() - 18, heute.getMonth(), heute
			.getDate());
	if (geb > mindestDatum && geb <= maxDatum) {
		document.getElementById("gebText").innerHTML = "";
		return true;
	} else if (geb > maxDatum && geb <= heute) {
		document.getElementById("gebText").innerHTML = "&cross; Die Registrierung ist erst ab 18 Jahren gestattet!";
		return false;
	} else {
		document.getElementById("gebText").innerHTML = "&cross; Bitte g&uuml;ltiges Datum eingeben!";
		return false;
	}
}
// Gleiches Passwort Validierung
function checkPass() {
	var passwort = document.getElementById("passwortRegister").value, passwort2 = document
			.getElementById("passwortRegister2").value;
	if (passwort === passwort2 && passwort !== "" && passwort2 !== "") {
		document.getElementById("passwortText").innerHTML = "";
		return true;
	} else if (passwort === "" || passwort2 === "") {
		document.getElementById("passwortText").innerHTML = "&cross; Bitte Passwort eingeben!";
		return false;
	} else {
		document.getElementById("passwortText").innerHTML = "&cross; Ihre Passw&ouml;rter stimmen nicht &uuml;berein!";
		return false;
	}
}
// CapsLock erkennung
function checkCaps() {

	if (event.getModifierState("CapsLock")) {
		document.getElementById("capsLockText").innerHTML = "&DoubleUpArrow; Achtung! Caps Lock ist aktiviert!";
	} else {
		document.getElementById("capsLockText").innerHTML = "";
	}
}
// Submit Form
function checkForm(event) {
	if (checkGeb() & checkPass() & checkMailBoolean) {
		return true;
	}
	else {
		event.preventDefault();
	}
}
