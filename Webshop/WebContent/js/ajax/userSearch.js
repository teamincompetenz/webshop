/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);
function init() {
	document.getElementById("nachname").addEventListener("keyup", changeContent);
	document.getElementById("mail").addEventListener("keyup", changeContent);
}

// AJAX-Suche der User in CRUD-User
function changeContent() {
	var searchURL = "../../ReadAllUserServlet";
	var nachname = document.getElementById("nachname").value;
	var mail = document.getElementById("mail").value;
	if (nachname != null && nachname.length > 0)
		searchURL += "?nachname=" + encodeURIComponent(nachname);
	if ((mail != null && mail.length > 0) && (nachname != null && nachname.length > 0)) {
		searchURL += "&mail=" + encodeURIComponent(mail);
	} else if (mail != null && mail.length > 0) { 
		searchURL += "?mail=" + encodeURIComponent(mail);
	}
	

	var xmlhttp = new XMLHttpRequest();
	xmlhttp.addEventListener("load", function() {
		document.getElementById("crudTable").innerHTML = xmlhttp.responseText;
	});
	xmlhttp.open("GET", searchURL, true);
	xmlhttp.send();
}
