/*
 * Verantwortlicher: Daniel Fina
 */

"use strict";
document.addEventListener("DOMContentLoaded", init);

function init() {
	document.getElementById("preis").addEventListener("change", changeContent);
	document.getElementById("farbe").addEventListener("change", changeContent);
	var suchButton = document.getElementsByClassName("suchButton");
	var i;
	for (i = 0; i < suchButton.length; i++) {
	  suchButton[i].addEventListener("click", changeContent);
	} 
}

// URL zusammensetzen je nach Value und an FilterServlet per AJAX schicken
function changeContent() {
	var searchURL = "../../FilterServlet";
	var kategorie = document.getElementById("kategorie").value;
	var preis = document.getElementById("preis").value;
	var minPreis = document.getElementById("minPreis").value;
	var maxPreis = document.getElementById("maxPreis").value;
	var farbe = document.getElementById("farbe").value;
	var suche = document.getElementById("suche").value;
	
	if (kategorie != null) {
		searchURL += "?kategorie=" + encodeURIComponent(kategorie);
	} 
	if (preis != null) {
		searchURL += "&preis=" + encodeURIComponent(preis);
	} 
	if (minPreis != null) {
		searchURL += "&minPreis=" + encodeURIComponent(minPreis);
	}
	if (maxPreis != null) {
		searchURL += "&maxPreis=" + encodeURIComponent(maxPreis);
	}
	if (farbe != null) {
		searchURL += "&farbe=" + encodeURIComponent(farbe);
	}
	if (suche != null) {
		searchURL += "&suche=" + encodeURIComponent(suche);
	}
	
	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.addEventListener("load", function() {
		document.getElementById("filterTable").innerHTML = xmlhttp.responseText;
	});
	xmlhttp.open("GET", searchURL, true);
	xmlhttp.send();
}